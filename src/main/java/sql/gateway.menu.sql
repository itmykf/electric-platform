INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1840579144334106625', 1839220886884757506, 'gateway', '设备网关', 'menu', '/manage/gateway', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1840579144334106626', '1840579144334106625', 'gateway_add', '新增', 'add', '/manage/gateway/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1840579144334106627', '1840579144334106625', 'gateway_edit', '修改', 'edit', '/manage/gateway/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1840579144334106628', '1840579144334106625', 'gateway_delete', '删除', 'delete', '/api/blade-gateway/gateway/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1840579144334106629', '1840579144334106625', 'gateway_view', '查看', 'view', '/manage/gateway/view', 'file-text', 4, 2, 2, 1, NULL, 0);
