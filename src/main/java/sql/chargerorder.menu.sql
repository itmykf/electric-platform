INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842083457908305922', 1842084619487223810, 'chargerOrder', '充电桩订单', 'menu', '/charger/chargerOrder', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842083457908305923', '1842083457908305922', 'chargerOrder_add', '新增', 'add', '/charger/chargerOrder/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842083457908305924', '1842083457908305922', 'chargerOrder_edit', '修改', 'edit', '/charger/chargerOrder/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842083457908305925', '1842083457908305922', 'chargerOrder_delete', '删除', 'delete', '/api/blade-chargerorder/chargerOrder/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842083457908305926', '1842083457908305922', 'chargerOrder_view', '查看', 'view', '/charger/chargerOrder/view', 'file-text', 4, 2, 2, 1, NULL, 0);
