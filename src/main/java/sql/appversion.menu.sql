INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1841669487544418306', 1123598815738675201, 'appVersion', '版本管理', 'menu', '/appversion/appVersion', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1841669487544418307', '1841669487544418306', 'appVersion_add', '新增', 'add', '/appversion/appVersion/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1841669487544418308', '1841669487544418306', 'appVersion_edit', '修改', 'edit', '/appversion/appVersion/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1841669487544418309', '1841669487544418306', 'appVersion_delete', '删除', 'delete', '/api/blade-appversion/appVersion/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1841669487544418310', '1841669487544418306', 'appVersion_view', '查看', 'view', '/appversion/appVersion/view', 'file-text', 4, 2, 2, 1, NULL, 0);
