INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842449135601410050', 1123598815738675201, 'chargeOrderBattery', '订单电池信息', 'menu', '/charger/chargeOrderBattery', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842449135601410051', '1842449135601410050', 'chargeOrderBattery_add', '新增', 'add', '/charger/chargeOrderBattery/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842449135601410052', '1842449135601410050', 'chargeOrderBattery_edit', '修改', 'edit', '/charger/chargeOrderBattery/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842449135601410053', '1842449135601410050', 'chargeOrderBattery_delete', '删除', 'delete', '/api/blade-chargeorderbattery/chargeOrderBattery/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1842449135601410054', '1842449135601410050', 'chargeOrderBattery_view', '查看', 'view', '/charger/chargeOrderBattery/view', 'file-text', 4, 2, 2, 1, NULL, 0);
