INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843465932433805314', 1843466231672229890, 'productCategory', '产品分类', 'menu', '/thingsphere/productCategory', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843465932433805315', '1843465932433805314', 'productCategory_add', '新增', 'add', '/thingsphere/productCategory/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843465932433805316', '1843465932433805314', 'productCategory_edit', '修改', 'edit', '/thingsphere/productCategory/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843465932433805317', '1843465932433805314', 'productCategory_delete', '删除', 'delete', '/thingsphere/productCategory/remove', 'delete', 3, 2, 3, 1, NULL,
        0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843465932433805318', '1843465932433805314', 'productCategory_view', '查看', 'view', '/thingsphere/productCategory/view', 'file-text', 4, 2, 2, 1, NULL, 0);
