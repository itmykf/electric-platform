INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1839219521110323201', 1839220886884757506, 'protocol', '协议管理', 'menu', '/manage/protocol', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1839219521110323202', '1839219521110323201', 'protocol_add', '新增', 'add', '/manage/protocol/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1839219521110323203', '1839219521110323201', 'protocol_edit', '修改', 'edit', '/manage/protocol/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1839219521110323204', '1839219521110323201', 'protocol_delete', '删除', 'delete', '/api/blade-protocol/protocol/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1839219521110323205', '1839219521110323201', 'protocol_view', '查看', 'view', '/manage/protocol/view', 'file-text', 4, 2, 2, 1, NULL, 0);
