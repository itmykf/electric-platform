INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843469085560565762', 1843466231672229890, 'product', '产品', 'menu', '/thingsphere/product', NULL, 2, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843469085560565763', '1843469085560565762', 'product_add', '新增', 'add', '/thingsphere/product/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843469085560565764', '1843469085560565762', 'product_edit', '修改', 'edit', '/thingsphere/product/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843469085560565765', '1843469085560565762', 'product_delete', '删除', 'delete', '/thingsphere/product/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1843469085560565766', '1843469085560565762', 'product_view', '查看', 'view', '/thingsphere/product/view', 'file-text', 4, 2, 2, 1, NULL, 0);
