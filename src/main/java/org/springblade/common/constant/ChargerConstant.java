package org.springblade.common.constant;

/**
 * @author lhb
 * @date 2024/10/4 下午5:03
 */
public class ChargerConstant {

	public enum ChargerOrderStatus {
		DEVICE_STATUS(0),
		CLOUD_STATUS(1);

		public int getVal() {
			return val;
		}

		private int val;

		private ChargerOrderStatus(int val) {
			this.val = val;
		}
	}

	public enum ChargerOrderStatus2 {
		UNFINISHED(1),
		ALREADYENDED(2);

		public int getVal() {
			return val;
		}

		private int val;

		private ChargerOrderStatus2(int val) {
			this.val = val;
		}
	}

}
