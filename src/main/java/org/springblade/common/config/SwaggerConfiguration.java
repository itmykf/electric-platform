package org.springblade.common.config;

import lombok.AllArgsConstructor;
import org.springblade.core.launch.constant.AppConstant;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Swagger配置类
 *
 * @author Chill
 */
@Configuration(proxyBeanMethods = false)
@AllArgsConstructor
@ConditionalOnProperty(value = "swagger.enabled", havingValue = "true", matchIfMissing = true)
public class SwaggerConfiguration {

	@Bean
	public GroupedOpenApi authApi() {
		return GroupedOpenApi.builder()
			.group("授权模块")
			.packagesToScan(AppConstant.BASE_PACKAGES + ".core.oauth2", AppConstant.BASE_PACKAGES + ".modules.auth")
			.build();
	}

	@Bean
	public GroupedOpenApi sysApi() {
		return GroupedOpenApi.builder()
			.group("系统模块")
			.packagesToScan(AppConstant.BASE_PACKAGES + ".modules.system", AppConstant.BASE_PACKAGES + ".modules.resource")
			.build();
	}

	@Bean
	public GroupedOpenApi flowApi() {
		// 创建并返回GroupedOpenApi对象
		return GroupedOpenApi.builder()
			.group("工作流模块")
			.packagesToScan(AppConstant.BASE_PACKAGES + ".flow")
			.build();
	}

}
