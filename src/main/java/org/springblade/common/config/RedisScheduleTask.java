//package org.springblade.common.config;
//
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.dao.DataAccessException;
//import org.springframework.data.redis.connection.RedisConnection;
//import org.springframework.data.redis.core.RedisCallback;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
///**
// * @description:
// * @author: lhb
// * @create: 2023-06-08 13:19
// **/
//@Slf4j
//@Configuration
//@EnableScheduling
//@AllArgsConstructor
//public class RedisScheduleTask {
//
//	private StringRedisTemplate stringRedisTemplate;
//
//	@Scheduled(fixedRate = 30000)
//	public void configureTasks() {
//		log.debug("ping redis");
//		stringRedisTemplate.execute(new RedisCallback<String>() {
//			@Override
//			public String doInRedis(RedisConnection connection) throws DataAccessException {
//				log.debug("ping redis");
//				return connection.ping();
//			}
//		});
//	}
//}
