package org.springblade.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author lhb
 * @date 2024/9/27 下午4:40
 */
@Component
@Slf4j
public class LettuceConnectionValidTask {

	@Autowired
	private RedisConnectionFactory redisConnectionFactory;

	/**
	 * 每隔2秒校验异常lettuce连接是否正常，解决长期空闲lettuce连接关闭但是netty不能及时监控到的问题
	 */
	@Scheduled(cron = "0/5 * * * * ?")
	public void task() {
		if (redisConnectionFactory instanceof LettuceConnectionFactory) {
			LettuceConnectionFactory c = (LettuceConnectionFactory) redisConnectionFactory;
			c.validateConnection();
		}
	}
}
