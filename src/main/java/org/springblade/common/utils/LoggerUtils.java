package org.springblade.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lhb
 * @date 2024/10/9 下午6:31
 */
public class LoggerUtils {
	public static <T> Logger logger(Class<T> clazz) {
		return LoggerFactory.getLogger(clazz);
	}

	/**
	 * 打印到指定的文件下
	 *
	 * @param desc 日志文件名称
	 * @return
	 */
	public static Logger logger(LogFileName desc) {
		return LoggerFactory.getLogger(desc.getLogFileName());
	}
}
