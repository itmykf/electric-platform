package org.springblade.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author lhb
 * @date 2024/10/9 下午6:31
 */
public enum LogFileName {
	// 充电桩
	CHARGER("charger_log"),
	;

	private String logFileName;

	LogFileName(String fileName) {
		this.logFileName = fileName;
	}

	public String getLogFileName() {
		return logFileName;
	}

	public void setLogFileName(String logFileName) {
		this.logFileName = logFileName;
	}

	public static LogFileName getAwardTypeEnum(String value) {
		LogFileName[] arr = values();
		for (LogFileName item : arr) {
			if (null != item && StringUtils.isNotBlank(item.logFileName)) {
				return item;
			}
		}
		return null;
	}
}
