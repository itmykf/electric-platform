package org.springblade;

import org.dromara.easyes.starter.register.EsMapperScan;
import org.springblade.common.constant.CommonConstant;
import org.springblade.core.launch.BladeApplication;
import org.springblade.ocpp.send.DelaySend;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

/**
 * 启动器
 *
 * @author Chill
 */
@EsMapperScan("org.springblade.thingsphere.elasticsearch.mapper")
@EnableWebSocket
@EnableScheduling
@EnableRedisHttpSession
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		BladeApplication.run(CommonConstant.APPLICATION_NAME, Application.class, args);

		// Example Start the OCPP delay queue
		DelaySend.start();
	}
}

