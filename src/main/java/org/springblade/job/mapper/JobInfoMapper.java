package org.springblade.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.job.pojo.entity.JobInfo;
import org.springblade.job.pojo.vo.JobInfoVO;

import java.util.List;

/**
 * 任务信息表 Mapper 接口
 *
 * @author BladeX
 */
public interface JobInfoMapper extends BaseMapper<JobInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param jobInfo
	 * @return
	 */
	List<JobInfoVO> selectJobInfoPage(IPage page, JobInfoVO jobInfo);

}
