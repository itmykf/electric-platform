package org.springblade.modules.common.appversion.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.common.appversion.excel.AppVersionExcel;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;

import java.util.List;

/**
 * 版本管理 服务类
 *
 * @author zhs
 * @since 2024-10-03
 */
public interface IAppVersionService extends BaseService<AppVersionEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param appVersion
	 * @return
	 */
	IPage<AppVersionVO> selectAppVersionPage(IPage<AppVersionVO> page, AppVersionVO appVersion);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<AppVersionExcel> exportAppVersion(Wrapper<AppVersionEntity> queryWrapper);

}
