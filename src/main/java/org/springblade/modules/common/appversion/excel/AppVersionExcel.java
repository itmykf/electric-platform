package org.springblade.modules.common.appversion.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;


/**
 * 版本管理 Excel实体类
 *
 * @author zhs
 * @since 2024-10-03
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class AppVersionExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * download url
	 */
	@ColumnWidth(20)
	@ExcelProperty("download url")
	private String downloadLink;
	/**
	 * 1 normal update 2 force update
	 */
	@ColumnWidth(20)
	@ExcelProperty("1 normal update 2 force update")
	private Byte forceUpdate;
	/**
	 * version number
	 */
	@ColumnWidth(20)
	@ExcelProperty("version number")
	private String versionNum;
	/**
	 * version name
	 */
	@ColumnWidth(20)
	@ExcelProperty("version name")
	private String versionName;
	/**
	 * apk; station; cabinet; charger
	 */
	@ColumnWidth(20)
	@ExcelProperty("apk; station; cabinet; charger")
	private String type;
	/**
	 * public time
	 */
	@ColumnWidth(20)
	@ExcelProperty("public time")
	private Date publicTime;
	/**
	 * version description
	 */
	@ColumnWidth(20)
	@ExcelProperty("version description")
	private String description;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Date modifyTime;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long modifyUser;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Boolean delFlag;

}
