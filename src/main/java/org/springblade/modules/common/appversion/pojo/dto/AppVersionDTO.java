package org.springblade.modules.common.appversion.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;

import java.io.Serial;

/**
 * 版本管理 数据传输对象实体类
 *
 * @author zhs
 * @since 2024-10-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AppVersionDTO extends AppVersionEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
