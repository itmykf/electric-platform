package org.springblade.modules.common.appversion.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;
import java.util.Date;

/**
 * 版本管理 实体类
 *
 * @author zhs
 * @since 2024-10-03
 */
@Data
@TableName("t_app_version")
@Schema(description = "AppVersion对象")
@EqualsAndHashCode(callSuper = true)
public class AppVersionEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * download url
	 */
	@Schema(description = "download url")
	private String downloadLink;
	/**
	 * 1 normal update 2 force update
	 */
	@Schema(description = "1 normal update 2 force update")
	private Byte forceUpdate;
	/**
	 * version number
	 */
	@Schema(description = "version number")
	private String versionNum;
	/**
	 * version name
	 */
	@Schema(description = "version name")
	private String versionName;
	/**
	 * apk; station; cabinet; charger
	 */
	@Schema(description = "apk; station; cabinet; charger")
	private String type;
	/**
	 * public time
	 */
	@Schema(description = "public time")
	private Date publicTime;
	/**
	 * version description
	 */
	@Schema(description = "version description")
	private String description;
	/**
	 *
	 */
	@Schema(description = "")
	private Date modifyTime;
	/**
	 *
	 */
	@Schema(description = "")
	private Long modifyUser;
	/**
	 *
	 */
	@Schema(description = "")
	private Boolean delFlag;

	/**
	 * 是否已删除
	 */
	@TableLogic
	@Schema(description = "是否已删除")
	private Integer isDeleted;

}
