package org.springblade.modules.common.appversion.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;

import java.io.Serial;

/**
 * 版本管理 视图实体类
 *
 * @author zhs
 * @since 2024-10-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AppVersionVO extends AppVersionEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
