package org.springblade.modules.common.appversion.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;

import java.util.Objects;

/**
 * 版本管理 包装类,返回视图层所需的字段
 *
 * @author zhs
 * @since 2024-10-03
 */
public class AppVersionWrapper extends BaseEntityWrapper<AppVersionEntity, AppVersionVO> {

	public static AppVersionWrapper build() {
		return new AppVersionWrapper();
	}

	@Override
	public AppVersionVO entityVO(AppVersionEntity appVersion) {
		AppVersionVO appVersionVO = Objects.requireNonNull(BeanUtil.copyProperties(appVersion, AppVersionVO.class));

		//User createUser = UserCache.getUser(appVersion.getCreateUser());
		//User updateUser = UserCache.getUser(appVersion.getUpdateUser());
		//appVersionVO.setCreateUserName(createUser.getName());
		//appVersionVO.setUpdateUserName(updateUser.getName());

		return appVersionVO;
	}


}
