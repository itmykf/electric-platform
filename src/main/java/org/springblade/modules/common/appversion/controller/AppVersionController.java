package org.springblade.modules.common.appversion.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.common.appversion.excel.AppVersionExcel;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;
import org.springblade.modules.common.appversion.service.IAppVersionService;
import org.springblade.modules.common.appversion.wrapper.AppVersionWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 版本管理 控制器
 *
 * @author zhs
 * @since 2024-10-03
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-appversion/appVersion")
@Tag(name = "版本管理", description = "版本管理接口")
public class AppVersionController extends BladeController {

	private final IAppVersionService appVersionService;

	/**
	 * 版本管理 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入appVersion")
	public R<AppVersionVO> detail(AppVersionEntity appVersion) {
		AppVersionEntity detail = appVersionService.getOne(Condition.getQueryWrapper(appVersion));
		return R.data(AppVersionWrapper.build().entityVO(detail));
	}

	/**
	 * 版本管理 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入appVersion")
	public R<IPage<AppVersionVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> appVersion, Query query) {
		IPage<AppVersionEntity> pages = appVersionService.page(Condition.getPage(query), Condition.getQueryWrapper(appVersion, AppVersionEntity.class));
		return R.data(AppVersionWrapper.build().pageVO(pages));
	}

	/**
	 * 版本管理 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@Operation(summary = "分页", description = "传入appVersion")
	public R<IPage<AppVersionVO>> page(AppVersionVO appVersion, Query query) {
		IPage<AppVersionVO> pages = appVersionService.selectAppVersionPage(Condition.getPage(query), appVersion);
		return R.data(pages);
	}

	/**
	 * 版本管理 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@Operation(summary = "新增", description = "传入appVersion")
	public R save(@Valid @RequestBody AppVersionEntity appVersion) {
		return R.status(appVersionService.save(appVersion));
	}

	/**
	 * 版本管理 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@Operation(summary = "修改", description = "传入appVersion")
	public R update(@Valid @RequestBody AppVersionEntity appVersion) {
		return R.status(appVersionService.updateById(appVersion));
	}

	/**
	 * 版本管理 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入appVersion")
	public R submit(@Valid @RequestBody AppVersionEntity appVersion) {
		return R.status(appVersionService.saveOrUpdate(appVersion));
	}

	/**
	 * 版本管理 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(appVersionService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-appVersion")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入appVersion")
	public void exportAppVersion(@Parameter(hidden = true) @RequestParam Map<String, Object> appVersion, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<AppVersionEntity> queryWrapper = Condition.getQueryWrapper(appVersion, AppVersionEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(AppVersion::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(AppVersionEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<AppVersionExcel> list = appVersionService.exportAppVersion(queryWrapper);
		ExcelUtil.export(response, "版本管理数据" + DateUtil.time(), "版本管理数据表", list, AppVersionExcel.class);
	}

}
