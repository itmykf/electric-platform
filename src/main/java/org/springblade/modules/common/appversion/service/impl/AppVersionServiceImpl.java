package org.springblade.modules.common.appversion.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.common.appversion.excel.AppVersionExcel;
import org.springblade.modules.common.appversion.mapper.AppVersionMapper;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;
import org.springblade.modules.common.appversion.service.IAppVersionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版本管理 服务实现类
 *
 * @author zhs
 * @since 2024-10-03
 */
@Service
public class AppVersionServiceImpl extends BaseServiceImpl<AppVersionMapper, AppVersionEntity> implements IAppVersionService {

	@Override
	public IPage<AppVersionVO> selectAppVersionPage(IPage<AppVersionVO> page, AppVersionVO appVersion) {
		return page.setRecords(baseMapper.selectAppVersionPage(page, appVersion));
	}


	@Override
	public List<AppVersionExcel> exportAppVersion(Wrapper<AppVersionEntity> queryWrapper) {
		List<AppVersionExcel> appVersionList = baseMapper.exportAppVersion(queryWrapper);
		//appVersionList.forEach(appVersion -> {
		//	appVersion.setTypeName(DictCache.getValue(DictEnum.YES_NO, AppVersion.getType()));
		//});
		return appVersionList;
	}

}
