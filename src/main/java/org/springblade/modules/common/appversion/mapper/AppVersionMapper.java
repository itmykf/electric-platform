package org.springblade.modules.common.appversion.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.modules.common.appversion.excel.AppVersionExcel;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;

import java.util.List;

/**
 * 版本管理 Mapper 接口
 *
 * @author zhs
 * @since 2024-10-03
 */
public interface AppVersionMapper extends BaseMapper<AppVersionEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param appVersion
	 * @return
	 */
	List<AppVersionVO> selectAppVersionPage(IPage page, AppVersionVO appVersion);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<AppVersionExcel> exportAppVersion(@Param("ew") Wrapper<AppVersionEntity> queryWrapper);

}
