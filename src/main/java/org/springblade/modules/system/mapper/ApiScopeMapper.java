package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.ApiScope;

/**
 * Mapper 接口
 *
 * @author BladeX
 */
public interface ApiScopeMapper extends BaseMapper<ApiScope> {

}
