package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.TopMenu;

/**
 * 顶部菜单表 Mapper 接口
 *
 * @author BladeX
 */
public interface TopMenuMapper extends BaseMapper<TopMenu> {

}
