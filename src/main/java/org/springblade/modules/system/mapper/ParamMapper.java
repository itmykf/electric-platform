package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.Param;

/**
 * Mapper 接口
 *
 * @author Chill
 */
public interface ParamMapper extends BaseMapper<Param> {

}
