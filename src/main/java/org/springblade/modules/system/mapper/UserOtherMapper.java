package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.UserOther;

/**
 * Mapper 接口
 *
 * @author Chill
 */
public interface UserOtherMapper extends BaseMapper<UserOther> {


}
