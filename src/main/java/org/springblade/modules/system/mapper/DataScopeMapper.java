package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.DataScope;

/**
 * Mapper 接口
 *
 * @author BladeX
 */
public interface DataScopeMapper extends BaseMapper<DataScope> {

}
