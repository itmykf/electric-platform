package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.RoleScope;

/**
 * Mapper 接口
 *
 * @author Chill
 */
public interface RoleScopeMapper extends BaseMapper<RoleScope> {

}
