package org.springblade.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.system.pojo.entity.TenantPackage;

/**
 * 租户产品表 Mapper 接口
 *
 * @author BladeX
 */
public interface TenantPackageMapper extends BaseMapper<TenantPackage> {

}
