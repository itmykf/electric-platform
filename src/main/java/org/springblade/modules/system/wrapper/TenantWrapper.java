package org.springblade.modules.system.wrapper;

import org.springblade.core.tool.utils.Func;

import java.util.Map;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author Chill
 */
public class TenantWrapper {

	public static TenantWrapper build() {
		return new TenantWrapper();
	}

	/**
	 * 查询条件处理
	 */
	public void entityQuery(Map<String, Object> entity) {
		// 此场景仅在 pg数据库 map类型传参的情况下需要处理，entity传参已经包含数据类型，则无需关心
		// 针对 pg数据库 int类型字段查询需要强转的处理示例
		String searchKey = "status_equal";
		if (Func.isNotEmpty(entity.get(searchKey))) {
			// 数据库字段为int类型，设置"="查询，具体查询参数请见 @org.springblade.core.mp.support.SqlKeyword
			entity.put(searchKey, Func.toInt(entity.get(searchKey)));
		}
	}

}
