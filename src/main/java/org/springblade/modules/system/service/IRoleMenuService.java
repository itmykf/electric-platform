package org.springblade.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springblade.modules.system.pojo.entity.RoleMenu;

/**
 * 服务类
 *
 * @author Chill
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
