package org.springblade.modules.system.service.impl;

import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.system.mapper.TenantPackageMapper;
import org.springblade.modules.system.pojo.entity.TenantPackage;
import org.springblade.modules.system.service.ITenantPackageService;
import org.springframework.stereotype.Service;

/**
 * 租户产品表 服务实现类
 *
 * @author BladeX
 */
@Service
public class TenantPackageServiceImpl extends BaseServiceImpl<TenantPackageMapper, TenantPackage> implements ITenantPackageService {

}
