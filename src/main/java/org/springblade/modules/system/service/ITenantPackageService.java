package org.springblade.modules.system.service;

import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.system.pojo.entity.TenantPackage;

/**
 * 租户产品表 服务类
 *
 * @author BladeX
 */
public interface ITenantPackageService extends BaseService<TenantPackage> {

}
