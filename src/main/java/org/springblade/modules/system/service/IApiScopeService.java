package org.springblade.modules.system.service;

import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.system.pojo.entity.ApiScope;

/**
 * 服务类
 *
 * @author BladeX
 */
public interface IApiScopeService extends BaseService<ApiScope> {

}
