package org.springblade.modules.system.service;

import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.system.pojo.entity.DataScope;

/**
 * 服务类
 *
 * @author BladeX
 */
public interface IDataScopeService extends BaseService<DataScope> {

}
