package org.springblade.modules.system.rule;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.modules.system.pojo.entity.Role;
import org.springblade.modules.system.pojo.entity.Tenant;

/**
 * 租户角色构建
 *
 * @author Chill
 */
@LiteflowComponent(id = "tenantRoleRule", name = "租户角色构建")
public class TenantRoleRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		// 获取上下文
		TenantContext contextBean = this.getFirstContextBean();
		Tenant tenant = contextBean.getTenant();
		// 新建租户对应的默认角色
		Role role = new Role();
		role.setTenantId(tenant.getTenantId());
		role.setParentId(BladeConstant.TOP_PARENT_ID);
		role.setRoleName("管理员");
		role.setRoleAlias("admin");
		role.setSort(2);
		role.setIsDeleted(BladeConstant.DB_NOT_DELETED);
		// 设置上下文
		contextBean.setRole(role);
	}
}
