package org.springblade.modules.system.rule;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.modules.system.pojo.entity.Post;
import org.springblade.modules.system.pojo.entity.Tenant;

/**
 * 租户岗位构建
 *
 * @author Chill
 */
@LiteflowComponent(id = "tenantPostRule", name = "租户岗位构建")
public class TenantPostRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		// 获取上下文
		TenantContext contextBean = this.getFirstContextBean();
		Tenant tenant = contextBean.getTenant();

		// 新建租户对应的默认岗位
		Post post = new Post();
		post.setTenantId(tenant.getTenantId());
		post.setCategory(1);
		post.setPostCode("ceo");
		post.setPostName("首席执行官");
		post.setSort(1);

		// 设置上下文
		contextBean.setPost(post);

	}
}
