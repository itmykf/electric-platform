package org.springblade.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.cache.utils.CacheUtil;
import org.springblade.core.launch.constant.AppConstant;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tenant.annotation.NonDS;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.system.pojo.entity.ApiScope;
import org.springblade.modules.system.pojo.vo.ApiScopeVO;
import org.springblade.modules.system.service.IApiScopeService;
import org.springblade.modules.system.wrapper.ApiScopeWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.springblade.core.cache.constant.CacheConstant.SYS_CACHE;

/**
 * 接口权限控制器
 *
 * @author BladeX
 */
@NonDS
@RestController
@AllArgsConstructor
@RequestMapping(AppConstant.APPLICATION_SYSTEM_NAME + "/api-scope")
@Tag(name = "接口权限", description = "接口权限")
public class ApiScopeController extends BladeController {

	private final IApiScopeService apiScopeService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入dataScope")
	public R<ApiScope> detail(ApiScope dataScope) {
		ApiScope detail = apiScopeService.getOne(Condition.getQueryWrapper(dataScope));
		return R.data(detail);
	}

	/**
	 * 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入dataScope")
	public R<IPage<ApiScopeVO>> list(ApiScope dataScope, Query query) {
		IPage<ApiScope> pages = apiScopeService.page(Condition.getPage(query), Condition.getQueryWrapper(dataScope));
		return R.data(ApiScopeWrapper.build().pageVO(pages));
	}

	/**
	 * 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 3)
	@Operation(summary = "新增", description = "传入dataScope")
	public R save(@Valid @RequestBody ApiScope dataScope) {
		CacheUtil.clear(SYS_CACHE, Boolean.FALSE);
		return R.status(apiScopeService.save(dataScope));
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 4)
	@Operation(summary = "修改", description = "传入dataScope")
	public R update(@Valid @RequestBody ApiScope dataScope) {
		CacheUtil.clear(SYS_CACHE, Boolean.FALSE);
		return R.status(apiScopeService.updateById(dataScope));
	}

	/**
	 * 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 5)
	@Operation(summary = "新增或修改", description = "传入dataScope")
	public R submit(@Valid @RequestBody ApiScope dataScope) {
		CacheUtil.clear(SYS_CACHE, Boolean.FALSE);
		return R.status(apiScopeService.saveOrUpdate(dataScope));
	}


	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		CacheUtil.clear(SYS_CACHE, Boolean.FALSE);
		return R.status(apiScopeService.deleteLogic(Func.toLongList(ids)));
	}


	public static void main(String[] args) {
		Set<Long> set = new HashSet<>();
		set.addAll(
			Arrays.asList(1398541071804727297L,
				1398541435710930945L,
				1609078660847947778L,
				1609078911315005442L,
				1609079148570005506L,
				1609079331487797249L,
				1609079591954075650L,
				1609079986948460546L,
				1609083173638033410L,
				1398541071804727297L,
				1398541435710930945L,
				1398543822378905601L,
				1398544077174484994L,
				1398544335501668354L,
				1398544494188965889L,
				1609078911315005442L,
				1609079148570005506L,
				1609079331487797249L,
				1609079591954075650L,
				1609079986948460546L,
				1609080259590803458L,
				1609080481771474945L,
				1609080600646438913L,
				1609080797908750337L,
				1609080879437631489L,
				1609080950086488066L,
				1609081072866349058L,
				1609081198674497537L,
				1609081256916602882L,
				1609081316773515266L,
				1609081429474463745L,
				1609081603798126593L,
				1609081740796678146L,
				1609081800661979137L,
				1609081882870337537L,
				1609081994287828993L,
				1609082188911923201L,
				1609082306184663041L,
				1609082403555430402L,
				1609082505963556866L,
				1609083173638033410L,
				1609083377741254658L,
				1609083461371482114L,
				1609111955258265602L,
				1609112010673410050L,
				1609112085411713026L,
				1609112156593246209L,
				1310164325094952961L,
				1310164408498688002L,
				1310164501457047553L,
				1310164674082017281L,
				1310164861605154818L,
				1310165081608982529L,
				1310165789699772417L,
				1310165875842387970L,
				1310165947413991426L,
				1310167103821414402L,
				1310167251985203202L,
				1310167392796377090L,
				1310167546748305410L,
				1310176417751597057L,
				1310176523942985730L,
				1310176788943306754L,
				1310176875366940674L,
				1310177049912901633L,
				1310177133522157570L,
				1310178496213778434L,
				1310178620201598978L,
				1310178706478432258L,
				1310178864435920897L,
				1310178966076489729L,
				1310179167507939330L,
				1398541071804727297L,
				1398541435710930945L,
				1398543822378905601L,
				1398544077174484994L,
				1398544335501668354L,
				1398544494188965889L,
				1609078911315005442L,
				1609079148570005506L,
				1609079331487797249L,
				1609079591954075650L,
				1609079986948460546L,
				1609080259590803458L,
				1609080481771474945L,
				1609080600646438913L,
				1609080797908750337L,
				1609080879437631489L,
				1609080950086488066L,
				1609081072866349058L,
				1609081198674497537L,
				1609081256916602882L,
				1609081316773515266L,
				1609081429474463745L,
				1609081603798126593L,
				1609081740796678146L,
				1609081800661979137L,
				1609081882870337537L,
				1609081994287828993L,
				1609082188911923201L,
				1609082306184663041L,
				1609082403555430402L,
				1609082505963556866L,
				1609083173638033410L,
				1609083377741254658L,
				1609083461371482114L,
				1609111955258265602L,
				1609112010673410050L,
				1609112085411713026L,
				1609112156593246209L)
		);
		for (Long l : set) {
			System.out.println(l + "L,");
		}
	}


}
