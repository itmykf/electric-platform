package org.springblade.modules.desk.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;
import java.util.Date;

/**
 * 实体类
 *
 * @author Chill
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("blade_notice")
@Schema(description = "公告实体类")
public class Notice extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 标题
	 */
	@Schema(description = "标题")
	private String title;

	/**
	 * 通知类型
	 */
	@Schema(description = "通知类型")
	private Integer category;

	/**
	 * 发布日期
	 */
	@Schema(description = "发布日期")
	private Date releaseTime;

	/**
	 * 内容
	 */
	@Schema(description = "内容")
	private String content;


}
