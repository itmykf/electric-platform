package org.springblade.modules.charger.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.charger.excel.ChargerExcel;
import org.springblade.modules.charger.pojo.entity.ChargerEntity;
import org.springblade.modules.charger.pojo.req.StartingChargeRequestBody;
import org.springblade.modules.charger.pojo.req.UpdateFirmwareRequestBody;
import org.springblade.modules.charger.pojo.vo.ChargerVO;

import java.util.List;

/**
 * 充电桩管理 服务类
 *
 * @author lhb
 * @since 2024-10-04
 */
public interface IChargerService extends BaseService<ChargerEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param charger
	 * @return
	 */
	IPage<ChargerVO> selectChargerPage(IPage<ChargerVO> page, ChargerVO charger);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ChargerExcel> exportCharger(Wrapper<ChargerEntity> queryWrapper);

	/**
	 * 启动充电
	 *
	 * @param requestBody
	 * @return
	 */
	boolean startCharge(StartingChargeRequestBody requestBody);

	/**
	 * 停止充电
	 *
	 * @param requestBody
	 * @return
	 */
	boolean stopCharge(StartingChargeRequestBody requestBody);

	/**
	 * OTA升级
	 *
	 * @param requestBody
	 * @return
	 */
	boolean updateFirmware(UpdateFirmwareRequestBody requestBody);
}
