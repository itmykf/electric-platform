package org.springblade.modules.charger.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.charger.excel.ChargerOrderExcel;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;
import org.springblade.modules.charger.pojo.vo.ChargerOrderVO;

import java.util.List;

/**
 * 充电桩订单 服务类
 *
 * @author lhb
 * @since 2024-10-04
 */
public interface IChargerOrderService extends BaseService<ChargerOrderEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param chargerOrder
	 * @return
	 */
	IPage<ChargerOrderVO> selectChargerOrderPage(IPage<ChargerOrderVO> page, ChargerOrderVO chargerOrder);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ChargerOrderExcel> exportChargerOrder(Wrapper<ChargerOrderEntity> queryWrapper);

}
