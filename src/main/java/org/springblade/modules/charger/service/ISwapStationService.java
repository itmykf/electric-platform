package org.springblade.modules.charger.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.charger.excel.SwapStationExcel;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;
import org.springblade.modules.charger.pojo.vo.SwapStationVO;

import java.util.List;

/**
 * 换电柜 服务类
 *
 * @author ludada
 * @since 2024-09-29
 */
public interface ISwapStationService extends BaseService<SwapStationEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param swapStation
	 * @return
	 */
	IPage<SwapStationVO> selectSwapStationPage(IPage<SwapStationVO> page, SwapStationVO swapStation);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<SwapStationExcel> exportSwapStation(Wrapper<SwapStationEntity> queryWrapper);

}
