package org.springblade.modules.charger.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.charger.excel.ChargerOrderExcel;
import org.springblade.modules.charger.mapper.ChargerOrderMapper;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;
import org.springblade.modules.charger.pojo.vo.ChargerOrderVO;
import org.springblade.modules.charger.service.IChargerOrderService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 充电桩订单 服务实现类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Service
public class ChargerOrderServiceImpl extends BaseServiceImpl<ChargerOrderMapper, ChargerOrderEntity> implements IChargerOrderService {

	@Override
	public IPage<ChargerOrderVO> selectChargerOrderPage(IPage<ChargerOrderVO> page, ChargerOrderVO chargerOrder) {
		return page.setRecords(baseMapper.selectChargerOrderPage(page, chargerOrder));
	}


	@Override
	public List<ChargerOrderExcel> exportChargerOrder(Wrapper<ChargerOrderEntity> queryWrapper) {
		List<ChargerOrderExcel> chargerOrderList = baseMapper.exportChargerOrder(queryWrapper);
		//chargerOrderList.forEach(chargerOrder -> {
		//	chargerOrder.setTypeName(DictCache.getValue(DictEnum.YES_NO, ChargerOrder.getType()));
		//});
		return chargerOrderList;
	}

}
