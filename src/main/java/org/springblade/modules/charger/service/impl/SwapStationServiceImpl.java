package org.springblade.modules.charger.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.charger.excel.SwapStationExcel;
import org.springblade.modules.charger.mapper.SwapStationMapper;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;
import org.springblade.modules.charger.pojo.vo.SwapStationVO;
import org.springblade.modules.charger.service.ISwapStationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 换电柜 服务实现类
 *
 * @author ludada
 * @since 2024-09-29
 */
@Service
public class SwapStationServiceImpl extends BaseServiceImpl<SwapStationMapper, SwapStationEntity> implements ISwapStationService {

	@Override
	public IPage<SwapStationVO> selectSwapStationPage(IPage<SwapStationVO> page, SwapStationVO swapStation) {
		return page.setRecords(baseMapper.selectSwapStationPage(page, swapStation));
	}


	@Override
	public List<SwapStationExcel> exportSwapStation(Wrapper<SwapStationEntity> queryWrapper) {
		List<SwapStationExcel> swapStationList = baseMapper.exportSwapStation(queryWrapper);
		//swapStationList.forEach(swapStation -> {
		//	swapStation.setTypeName(DictCache.getValue(DictEnum.YES_NO, SwapStation.getType()));
		//});
		return swapStationList;
	}

}
