/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.modules.charger.service.impl;

import org.springblade.modules.charger.pojo.entity.ChargeOrderBatteryEntity;
import org.springblade.modules.charger.pojo.vo.ChargeOrderBatteryVO;
import org.springblade.modules.charger.excel.ChargeOrderBatteryExcel;
import org.springblade.modules.charger.mapper.ChargeOrderBatteryMapper;
import org.springblade.modules.charger.service.IChargeOrderBatteryService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import java.util.List;

/**
 * 订单电池信息 服务实现类
 *
 * @author lhb
 * @since 2024-10-05
 */
@Service
public class ChargeOrderBatteryServiceImpl extends BaseServiceImpl<ChargeOrderBatteryMapper, ChargeOrderBatteryEntity> implements IChargeOrderBatteryService {

	@Override
	public IPage<ChargeOrderBatteryVO> selectChargeOrderBatteryPage(IPage<ChargeOrderBatteryVO> page, ChargeOrderBatteryVO chargeOrderBattery) {
		return page.setRecords(baseMapper.selectChargeOrderBatteryPage(page, chargeOrderBattery));
	}


	@Override
	public List<ChargeOrderBatteryExcel> exportChargeOrderBattery(Wrapper<ChargeOrderBatteryEntity> queryWrapper) {
		List<ChargeOrderBatteryExcel> chargeOrderBatteryList = baseMapper.exportChargeOrderBattery(queryWrapper);
		//chargeOrderBatteryList.forEach(chargeOrderBattery -> {
		//	chargeOrderBattery.setTypeName(DictCache.getValue(DictEnum.YES_NO, ChargeOrderBattery.getType()));
		//});
		return chargeOrderBatteryList;
	}

}
