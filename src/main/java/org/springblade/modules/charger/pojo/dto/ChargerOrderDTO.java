package org.springblade.modules.charger.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;

import java.io.Serial;

/**
 * 充电桩订单 数据传输对象实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChargerOrderDTO extends ChargerOrderEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
