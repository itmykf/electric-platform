/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.modules.charger.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 订单电池信息 实体类
 *
 * @author lhb
 * @since 2024-10-05
 */
@Data
@TableName("t_charge_order_battery")
@Schema(description = "ChargeOrderBattery对象")
@EqualsAndHashCode(callSuper = true)
public class ChargeOrderBatteryEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 电池SN
	 */
	@Schema(description = "电池SN")
	private String batterySn;
	/**
	 * 充电桩编号
	 */
	@Schema(description = "充电桩编号")
	private String chargerSn;
	/**
	 * 充电枪
	 */
	@Schema(description = "充电枪")
	private Integer gunNum;
	/**
	 * 电池SOC信息
	 */
	@Schema(description = "电池SOC信息")
	private String socContent;

}
