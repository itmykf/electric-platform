package org.springblade.modules.charger.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/4 下午2:43
 */
@Data
public class MeterValuesDTO {
	@JsonProperty("value")
	private String value;
	@JsonProperty("context")
	private String context;
	@JsonProperty("format")
	private String format;
	@JsonProperty("measurand")
	private String measurand;
	@JsonProperty("phase")
	private String phase;
	@JsonProperty("location")
	private String location;
	@JsonProperty("unit")
	private String unit;
}
