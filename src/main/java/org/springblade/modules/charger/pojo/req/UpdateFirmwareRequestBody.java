package org.springblade.modules.charger.pojo.req;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/7 下午3:06
 */
@Data
public class UpdateFirmwareRequestBody {

	private Long id;
	private String chargerSn;
	private Long firmwareId;
}
