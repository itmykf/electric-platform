package org.springblade.modules.charger.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.charger.pojo.entity.ChargerEntity;
import org.springblade.modules.charger.pojo.vo.Inside.*;

import java.io.Serial;

/**
 * 充电桩管理 视图实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChargerVO extends ChargerEntity {
	@Serial
	private static final long serialVersionUID = 1L;

	private ChargingOrderJsonVO chargingOrderJson;
	private CurrentJsonVO currentJson;
	private VoltageJsonVO voltageJson;
	private PowerJsonVO powerJson;
	private StatusJsonVO statusJsonJson;
}
