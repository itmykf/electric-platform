package org.springblade.modules.charger.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serial;
import java.time.LocalDateTime;

/**
 * 充电桩订单 实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@TableName("t_charger_order")
@Schema(description = "ChargerOrder对象")
@EqualsAndHashCode(callSuper = true)
public class ChargerOrderEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	@Schema(description = "主键id")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 充电桩编号
	 */
	@Schema(description = "充电桩编号")
	private String chargerSn;
	/**
	 * 充电枪号
	 */
	@Schema(description = "充电枪号")
	private Integer gunNum;
	/**
	 * 开始充电时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Schema(description = "开始充电时间")
	private LocalDateTime startTime;
	/**
	 * 结束充电时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Schema(description = "结束充电时间")
	private LocalDateTime endTime;
	/**
	 * 岂动方式 0设备发起 1云端发起 关联字典表
	 */
	@Schema(description = "岂动方式 0设备发起 1云端发起 关联字典表")
	private Integer startup;
	/**
	 * 结束方式 0设备发起 1云端发起  关联字典表
	 */
	@Schema(description = "结束方式 0设备发起 1云端发起  关联字典表")
	private Integer shutdown;

	private String idTag;

	private Integer meterStart;

	private Integer meterStop;

}
