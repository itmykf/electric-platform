package org.springblade.modules.charger.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 换电柜 实体类
 *
 * @author ludada
 * @since 2024-09-29
 */
@Data
@TableName("iot_swap_station")
@Schema(description = "SwapStation对象")
@EqualsAndHashCode(callSuper = true)
public class SwapStationEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * belong country
	 */
	@Schema(description = "belong country")
	private String country;
	/**
	 * country iso 3166 code
	 */
	@Schema(description = "country iso 3166 code")
	private String countryIso;
	/**
	 * time zone
	 */
	@Schema(description = "time zone")
	private String timezone;
	/**
	 *
	 */
	@Schema(description = "")
	private String name;
	/**
	 * address
	 */
	@Schema(description = "address")
	private String address;
	/**
	 * longitude
	 */
	@Schema(description = "longitude")
	private BigDecimal lng;
	/**
	 * latitude
	 */
	@Schema(description = "latitude")
	private BigDecimal lat;
	/**
	 * swap station code
	 */
	@Schema(description = "swap station code")
	private String hdgCode;
	/**
	 * the factory
	 */
	@Schema(description = "the factory")
	private String factory;
	/**
	 * model
	 */
	@Schema(description = "model")
	private String model;
	/**
	 * order
	 */
	@Schema(description = "order")
	private Integer orderNum;
	/**
	 *
	 */
	@Schema(description = "")
	private Long operatorId;
	/**
	 *
	 */
	@Schema(description = "")
	private Long agentId;
	/**
	 * 0 is online, 1 is offline
	 */
	@Schema(description = "0 is online, 1 is offline")
	private Byte online;
	/**
	 * 0 is normal, 1 is no use
	 */
	@Schema(description = "0 is normal, 1 is no use")
	private Byte state;
	/**
	 * status code
	 */
	@Schema(description = "status code")
	private String errorCode;
	/**
	 * swap station login pwd default value 123456
	 */
	@Schema(description = "swap station login pwd default value 123456")
	private String managePwd;
	/**
	 * fan status
	 */
	@Schema(description = "fan status")
	private String fanStatus;
	/**
	 * power status
	 */
	@Schema(description = "power status")
	private String powerStatus;
	/**
	 * emergency status
	 */
	@Schema(description = "emergency status")
	private String emergencyStatus;
	/**
	 * water level status
	 */
	@Schema(description = "water level status")
	private String waterLevel;
	/**
	 *
	 */
	@Schema(description = "")
	private String appVersion;
	/**
	 * station version
	 */
	@Schema(description = "station version")
	private String stationVersion;
	/**
	 * cabinet version
	 */
	@Schema(description = "cabinet version")
	private String cabinetVersion;
	/**
	 * charge vesrion
	 */
	@Schema(description = "charge vesrion")
	private String chargerVersion;
	/**
	 * fire status
	 */
	@Schema(description = "fire status")
	private String fireStatus;
	/**
	 * air conditioner status
	 */
	@Schema(description = "air conditioner status")
	private String airConditionerStatus;
	/**
	 * back door open or close
	 */
	@Schema(description = "back door open or close")
	private String backDoor;
	/**
	 * Fleet
	 */
	@Schema(description = "Fleet")
	private Long fleetId;
	/**
	 * brightness time
	 */
	@Schema(description = "brightness time")
	private String timeSplot;
	/**
	 * station temperature
	 */
	@Schema(description = "station temperature")
	private String temperature;
	/**
	 * 0 is normal, 1 is delete
	 */
	@Schema(description = "0 is normal, 1 is delete")
	private String delFlag;
	/**
	 *
	 */
	@Schema(description = "")
	private Long createBy;
	/**
	 *
	 */
	@Schema(description = "")
	private Long updateBy;
	/**
	 * station local time
	 */
	@Schema(description = "station local time")
	private Date localTime;
	/**
	 *
	 */
	@Schema(description = "")
	private Long ownerId;
	/**
	 *
	 */
	@Schema(description = "")
	private String city;

}
