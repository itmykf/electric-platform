package org.springblade.modules.charger.pojo.vo.Inside;

import lombok.Data;
import org.springblade.modules.charger.pojo.dto.MeterValuesDTO;

import java.util.List;

/**
 * @author lhb
 * @date 2024/10/4 下午2:44
 */
@Data
public class PowerJsonVO {

	private List<MeterValuesDTO> gunOne;
	private List<MeterValuesDTO> gunTwo;
}
