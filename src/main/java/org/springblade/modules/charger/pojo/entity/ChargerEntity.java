package org.springblade.modules.charger.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serial;
import java.util.Date;

/**
 * 充电桩管理 实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@TableName("t_charger")
@Schema(description = "Charger对象")
@EqualsAndHashCode(callSuper = true)
public class ChargerEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	@Schema(description = "主键id")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	/**
	 * 充电桩名称
	 */
	@Schema(description = "充电桩名称")
	private String chargerName;
	/**
	 * 充电桩编号
	 */
	@Schema(description = "充电桩编号")
	private String chargerSn;
	/**
	 * 当前充电订单
	 */
	@Schema(description = "当前充电订单")
	private String chargingOrder;
	/**
	 * 当前开启的充电枪电流,json存储
	 */
	@Schema(description = "当前开启的充电枪电流,json存储")
	@TableField(value = "`current`")
	private String current;
	/**
	 * 当前开启的充电枪电压,json存储
	 */
	@Schema(description = "当前开启的充电枪电压,json存储")
	private String voltage;
	/**
	 * 当前开启的充电枪功率,json存储
	 */
	@Schema(description = "当前开启的充电枪功率,json存储")
	private String power;
	/**
	 * 所在地址
	 */
	@Schema(description = "所在地址")
	private String address;
	/**
	 * 双枪充电状态,json存储
	 */
	@Schema(description = "双枪充电状态,json存储")
	private String statusJson;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss z")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss z")
	@Schema(description = "创建时间", hidden = true)
	private Date createTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss z")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss z")
	@Schema(description = "更新时间", hidden = true)
	private Date updateTime;

}
