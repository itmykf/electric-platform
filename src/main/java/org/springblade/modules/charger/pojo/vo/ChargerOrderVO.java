package org.springblade.modules.charger.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;

import java.io.Serial;

/**
 * 充电桩订单 视图实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChargerOrderVO extends ChargerOrderEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
