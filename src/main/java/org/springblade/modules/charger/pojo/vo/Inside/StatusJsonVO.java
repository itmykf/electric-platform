package org.springblade.modules.charger.pojo.vo.Inside;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/10/4 下午2:45
 */
@NoArgsConstructor
@Data
public class StatusJsonVO {

	@JsonProperty("gunOne")
	private String gunOne;
	@JsonProperty("gunTwo")
	private String gunTwo;
}
