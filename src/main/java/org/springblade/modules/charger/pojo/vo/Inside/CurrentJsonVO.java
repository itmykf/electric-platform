package org.springblade.modules.charger.pojo.vo.Inside;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springblade.modules.charger.pojo.dto.MeterValuesDTO;

import java.util.List;

/**
 * @author lhb
 * @date 2024/10/4 下午2:34
 */
@NoArgsConstructor
@Data
public class CurrentJsonVO {

	private List<MeterValuesDTO> gunOne;
	private List<MeterValuesDTO> gunTwo;
}
