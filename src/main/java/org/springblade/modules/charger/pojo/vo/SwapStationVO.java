package org.springblade.modules.charger.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;

import java.io.Serial;

/**
 * 换电柜 视图实体类
 *
 * @author ludada
 * @since 2024-09-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SwapStationVO extends SwapStationEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
