package org.springblade.modules.charger.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.modules.charger.excel.SwapStationExcel;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;
import org.springblade.modules.charger.pojo.vo.SwapStationVO;

import java.util.List;

/**
 * 换电柜 Mapper 接口
 *
 * @author ludada
 * @since 2024-09-29
 */
public interface SwapStationMapper extends BaseMapper<SwapStationEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param swapStation
	 * @return
	 */
	List<SwapStationVO> selectSwapStationPage(IPage page, SwapStationVO swapStation);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<SwapStationExcel> exportSwapStation(@Param("ew") Wrapper<SwapStationEntity> queryWrapper);

}
