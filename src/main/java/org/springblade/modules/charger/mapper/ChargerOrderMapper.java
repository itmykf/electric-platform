package org.springblade.modules.charger.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.modules.charger.excel.ChargerOrderExcel;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;
import org.springblade.modules.charger.pojo.vo.ChargerOrderVO;

import java.util.List;

/**
 * 充电桩订单 Mapper 接口
 *
 * @author lhb
 * @since 2024-10-04
 */
public interface ChargerOrderMapper extends BaseMapper<ChargerOrderEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param chargerOrder
	 * @return
	 */
	List<ChargerOrderVO> selectChargerOrderPage(IPage page, ChargerOrderVO chargerOrder);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ChargerOrderExcel> exportChargerOrder(@Param("ew") Wrapper<ChargerOrderEntity> queryWrapper);

}
