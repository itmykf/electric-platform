package org.springblade.modules.charger.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.modules.charger.excel.ChargerExcel;
import org.springblade.modules.charger.pojo.entity.ChargerEntity;
import org.springblade.modules.charger.pojo.vo.ChargerVO;

import java.util.List;

/**
 * 充电桩管理 Mapper 接口
 *
 * @author lhb
 * @since 2024-10-04
 */
public interface ChargerMapper extends BaseMapper<ChargerEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param charger
	 * @return
	 */
	List<ChargerVO> selectChargerPage(IPage page, ChargerVO charger);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ChargerExcel> exportCharger(@Param("ew") Wrapper<ChargerEntity> queryWrapper);

}
