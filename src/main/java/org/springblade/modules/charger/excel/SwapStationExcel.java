package org.springblade.modules.charger.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 换电柜 Excel实体类
 *
 * @author ludada
 * @since 2024-09-29
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class SwapStationExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * belong country
	 */
	@ColumnWidth(20)
	@ExcelProperty("belong country")
	private String country;
	/**
	 * country iso 3166 code
	 */
	@ColumnWidth(20)
	@ExcelProperty("country iso 3166 code")
	private String countryIso;
	/**
	 * time zone
	 */
	@ColumnWidth(20)
	@ExcelProperty("time zone")
	private String timezone;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String name;
	/**
	 * address
	 */
	@ColumnWidth(20)
	@ExcelProperty("address")
	private String address;
	/**
	 * longitude
	 */
	@ColumnWidth(20)
	@ExcelProperty("longitude")
	private BigDecimal lng;
	/**
	 * latitude
	 */
	@ColumnWidth(20)
	@ExcelProperty("latitude")
	private BigDecimal lat;
	/**
	 * swap station code
	 */
	@ColumnWidth(20)
	@ExcelProperty("swap station code")
	private String hdgCode;
	/**
	 * the factory
	 */
	@ColumnWidth(20)
	@ExcelProperty("the factory")
	private String factory;
	/**
	 * model
	 */
	@ColumnWidth(20)
	@ExcelProperty("model")
	private String model;
	/**
	 * order
	 */
	@ColumnWidth(20)
	@ExcelProperty("order")
	private Integer orderNum;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long operatorId;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long agentId;
	/**
	 * 0 is online, 1 is offline
	 */
	@ColumnWidth(20)
	@ExcelProperty("0 is online, 1 is offline")
	private Byte online;
	/**
	 * 0 is normal, 1 is no use
	 */
	@ColumnWidth(20)
	@ExcelProperty("0 is normal, 1 is no use")
	private Byte state;
	/**
	 * status code
	 */
	@ColumnWidth(20)
	@ExcelProperty("status code")
	private String errorCode;
	/**
	 * swap station login pwd default value 123456
	 */
	@ColumnWidth(20)
	@ExcelProperty("swap station login pwd default value 123456")
	private String managePwd;
	/**
	 * fan status
	 */
	@ColumnWidth(20)
	@ExcelProperty("fan status")
	private String fanStatus;
	/**
	 * power status
	 */
	@ColumnWidth(20)
	@ExcelProperty("power status")
	private String powerStatus;
	/**
	 * emergency status
	 */
	@ColumnWidth(20)
	@ExcelProperty("emergency status")
	private String emergencyStatus;
	/**
	 * water level status
	 */
	@ColumnWidth(20)
	@ExcelProperty("water level status")
	private String waterLevel;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String appVersion;
	/**
	 * station version
	 */
	@ColumnWidth(20)
	@ExcelProperty("station version")
	private String stationVersion;
	/**
	 * cabinet version
	 */
	@ColumnWidth(20)
	@ExcelProperty("cabinet version")
	private String cabinetVersion;
	/**
	 * charge vesrion
	 */
	@ColumnWidth(20)
	@ExcelProperty("charge vesrion")
	private String chargerVersion;
	/**
	 * fire status
	 */
	@ColumnWidth(20)
	@ExcelProperty("fire status")
	private String fireStatus;
	/**
	 * air conditioner status
	 */
	@ColumnWidth(20)
	@ExcelProperty("air conditioner status")
	private String airConditionerStatus;
	/**
	 * back door open or close
	 */
	@ColumnWidth(20)
	@ExcelProperty("back door open or close")
	private String backDoor;
	/**
	 * Fleet
	 */
	@ColumnWidth(20)
	@ExcelProperty("Fleet")
	private Long fleetId;
	/**
	 * brightness time
	 */
	@ColumnWidth(20)
	@ExcelProperty("brightness time")
	private String timeSplot;
	/**
	 * station temperature
	 */
	@ColumnWidth(20)
	@ExcelProperty("station temperature")
	private String temperature;
	/**
	 * 0 is normal, 1 is delete
	 */
	@ColumnWidth(20)
	@ExcelProperty("0 is normal, 1 is delete")
	private String delFlag;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long createBy;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long updateBy;
	/**
	 * station local time
	 */
	@ColumnWidth(20)
	@ExcelProperty("station local time")
	private Date localTime;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private Long ownerId;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String city;

}
