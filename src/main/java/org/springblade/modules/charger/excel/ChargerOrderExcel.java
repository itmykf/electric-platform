package org.springblade.modules.charger.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;


/**
 * 充电桩订单 Excel实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ChargerOrderExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 充电桩编号
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电桩编号")
	private String chargerSn;
	/**
	 * 充电枪号
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电枪号")
	private String gunNum;
	/**
	 * 开始充电时间
	 */
	@ColumnWidth(20)
	@ExcelProperty("开始充电时间")
	private Date startTime;
	/**
	 * 结束充电时间
	 */
	@ColumnWidth(20)
	@ExcelProperty("结束充电时间")
	private Date endTime;
	/**
	 * 岂动方式 0设备发起 1云端发起 关联字典表
	 */
	@ColumnWidth(20)
	@ExcelProperty("岂动方式 0设备发起 1云端发起 关联字典表")
	private Integer startup;
	/**
	 * 结束方式 0设备发起 1云端发起  关联字典表
	 */
	@ColumnWidth(20)
	@ExcelProperty("结束方式 0设备发起 1云端发起  关联字典表")
	private Integer shutdown;
	/**
	 * 是否已删除 0 正常 1已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除 0 正常 1已删除")
	private Integer isDeleted;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;

}
