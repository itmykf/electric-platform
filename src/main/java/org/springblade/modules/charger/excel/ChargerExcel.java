package org.springblade.modules.charger.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 充电桩管理 Excel实体类
 *
 * @author lhb
 * @since 2024-10-04
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ChargerExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 充电桩名称
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电桩名称")
	private String chargerName;
	/**
	 * 充电桩编号
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电桩编号")
	private String chargerSn;
	/**
	 * 当前充电订单
	 */
	@ColumnWidth(20)
	@ExcelProperty("当前充电订单")
	private String chargingOrder;
	/**
	 * 当前开启的充电枪电流,json存储
	 */
	@ColumnWidth(20)
	@ExcelProperty("当前开启的充电枪电流,json存储")
	private String current;
	/**
	 * 当前开启的充电枪电压,json存储
	 */
	@ColumnWidth(20)
	@ExcelProperty("当前开启的充电枪电压,json存储")
	private String voltage;
	/**
	 * 当前开启的充电枪功率,json存储
	 */
	@ColumnWidth(20)
	@ExcelProperty("当前开启的充电枪功率,json存储")
	private String power;
	/**
	 * 所在地址
	 */
	@ColumnWidth(20)
	@ExcelProperty("所在地址")
	private String address;
	/**
	 * 双枪充电状态,json存储
	 */
	@ColumnWidth(20)
	@ExcelProperty("双枪充电状态,json存储")
	private String statusJson;
	/**
	 * 是否已删除 0 正常 1已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除 0 正常 1已删除")
	private Integer isDeleted;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;

}
