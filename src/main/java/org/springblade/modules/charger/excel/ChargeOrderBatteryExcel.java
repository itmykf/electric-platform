/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.modules.charger.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 订单电池信息 Excel实体类
 *
 * @author lhb
 * @since 2024-10-05
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ChargeOrderBatteryExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 电池SN
	 */
	@ColumnWidth(20)
	@ExcelProperty("电池SN")
	private String batterySn;
	/**
	 * 充电桩编号
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电桩编号")
	private String chargerSn;
	/**
	 * 充电枪
	 */
	@ColumnWidth(20)
	@ExcelProperty("充电枪")
	private Integer gunNum;
	/**
	 * 电池SOC信息
	 */
	@ColumnWidth(20)
	@ExcelProperty("电池SOC信息")
	private String socContent;
	/**
	 * 是否已删除 0 正常 1已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除 0 正常 1已删除")
	private Byte isDeleted;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;

}
