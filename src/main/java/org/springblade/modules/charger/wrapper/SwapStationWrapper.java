package org.springblade.modules.charger.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;
import org.springblade.modules.charger.pojo.vo.SwapStationVO;

import java.util.Objects;

/**
 * 换电柜 包装类,返回视图层所需的字段
 *
 * @author ludada
 * @since 2024-09-29
 */
public class SwapStationWrapper extends BaseEntityWrapper<SwapStationEntity, SwapStationVO> {

	public static SwapStationWrapper build() {
		return new SwapStationWrapper();
	}

	@Override
	public SwapStationVO entityVO(SwapStationEntity swapStation) {
		SwapStationVO swapStationVO = Objects.requireNonNull(BeanUtil.copyProperties(swapStation, SwapStationVO.class));

		//User createUser = UserCache.getUser(swapStation.getCreateUser());
		//User updateUser = UserCache.getUser(swapStation.getUpdateUser());
		//swapStationVO.setCreateUserName(createUser.getName());
		//swapStationVO.setUpdateUserName(updateUser.getName());

		return swapStationVO;
	}


}
