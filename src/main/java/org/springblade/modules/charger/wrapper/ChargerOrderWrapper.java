package org.springblade.modules.charger.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;
import org.springblade.modules.charger.pojo.vo.ChargerOrderVO;

import java.util.Objects;

/**
 * 充电桩订单 包装类,返回视图层所需的字段
 *
 * @author lhb
 * @since 2024-10-04
 */
public class ChargerOrderWrapper extends BaseEntityWrapper<ChargerOrderEntity, ChargerOrderVO> {

	public static ChargerOrderWrapper build() {
		return new ChargerOrderWrapper();
	}

	@Override
	public ChargerOrderVO entityVO(ChargerOrderEntity chargerOrder) {
		ChargerOrderVO chargerOrderVO = Objects.requireNonNull(BeanUtil.copyProperties(chargerOrder, ChargerOrderVO.class));

		//User createUser = UserCache.getUser(chargerOrder.getCreateUser());
		//User updateUser = UserCache.getUser(chargerOrder.getUpdateUser());
		//chargerOrderVO.setCreateUserName(createUser.getName());
		//chargerOrderVO.setUpdateUserName(updateUser.getName());

		return chargerOrderVO;
	}


}
