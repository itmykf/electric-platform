package org.springblade.modules.charger.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.modules.charger.pojo.entity.ChargerEntity;
import org.springblade.modules.charger.pojo.vo.ChargerVO;

import java.util.Objects;

/**
 * 充电桩管理 包装类,返回视图层所需的字段
 *
 * @author lhb
 * @since 2024-10-04
 */
public class ChargerWrapper extends BaseEntityWrapper<ChargerEntity, ChargerVO> {

	public static ChargerWrapper build() {
		return new ChargerWrapper();
	}

	@Override
	public ChargerVO entityVO(ChargerEntity charger) {
		ChargerVO chargerVO = Objects.requireNonNull(BeanUtil.copyProperties(charger, ChargerVO.class));

		//User createUser = UserCache.getUser(charger.getCreateUser());
		//User updateUser = UserCache.getUser(charger.getUpdateUser());
		//chargerVO.setCreateUserName(createUser.getName());
		//chargerVO.setUpdateUserName(updateUser.getName());

		return chargerVO;
	}


}
