package org.springblade.modules.charger.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.charger.excel.ChargerOrderExcel;
import org.springblade.modules.charger.pojo.entity.ChargerOrderEntity;
import org.springblade.modules.charger.pojo.vo.ChargerOrderVO;
import org.springblade.modules.charger.service.IChargerOrderService;
import org.springblade.modules.charger.wrapper.ChargerOrderWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 充电桩订单 控制器
 *
 * @author lhb
 * @since 2024-10-04
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-chargerorder/chargerOrder")
@Tag(name = "充电桩订单", description = "充电桩订单接口")
public class ChargerOrderController extends BladeController {

	private final IChargerOrderService chargerOrderService;

	/**
	 * 充电桩订单 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入chargerOrder")
	public R<ChargerOrderVO> detail(ChargerOrderEntity chargerOrder) {
		ChargerOrderEntity detail = chargerOrderService.getOne(Condition.getQueryWrapper(chargerOrder));
		return R.data(ChargerOrderWrapper.build().entityVO(detail));
	}

	/**
	 * 充电桩订单 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入chargerOrder")
	public R<IPage<ChargerOrderVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> chargerOrder, Query query) {
		IPage<ChargerOrderEntity> pages = chargerOrderService.page(Condition.getPage(query), Condition.getQueryWrapper(chargerOrder, ChargerOrderEntity.class));
		return R.data(ChargerOrderWrapper.build().pageVO(pages));
	}

	/**
	 * 充电桩订单 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@Operation(summary = "分页", description = "传入chargerOrder")
	public R<IPage<ChargerOrderVO>> page(ChargerOrderVO chargerOrder, Query query) {
		IPage<ChargerOrderVO> pages = chargerOrderService.selectChargerOrderPage(Condition.getPage(query), chargerOrder);
		return R.data(pages);
	}

	/**
	 * 充电桩订单 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@Operation(summary = "新增", description = "传入chargerOrder")
	public R save(@Valid @RequestBody ChargerOrderEntity chargerOrder) {
		return R.status(chargerOrderService.save(chargerOrder));
	}

	/**
	 * 充电桩订单 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@Operation(summary = "修改", description = "传入chargerOrder")
	public R update(@Valid @RequestBody ChargerOrderEntity chargerOrder) {
		return R.status(chargerOrderService.updateById(chargerOrder));
	}

	/**
	 * 充电桩订单 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入chargerOrder")
	public R submit(@Valid @RequestBody ChargerOrderEntity chargerOrder) {
		return R.status(chargerOrderService.saveOrUpdate(chargerOrder));
	}

	/**
	 * 充电桩订单 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(chargerOrderService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-chargerOrder")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入chargerOrder")
	public void exportChargerOrder(@Parameter(hidden = true) @RequestParam Map<String, Object> chargerOrder, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<ChargerOrderEntity> queryWrapper = Condition.getQueryWrapper(chargerOrder, ChargerOrderEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(ChargerOrder::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(ChargerOrderEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<ChargerOrderExcel> list = chargerOrderService.exportChargerOrder(queryWrapper);
		ExcelUtil.export(response, "充电桩订单数据" + DateUtil.time(), "充电桩订单数据表", list, ChargerOrderExcel.class);
	}

}
