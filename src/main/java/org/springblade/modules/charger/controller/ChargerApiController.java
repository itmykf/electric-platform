package org.springblade.modules.charger.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.tool.api.R;
import org.springblade.modules.common.appversion.pojo.entity.AppVersionEntity;
import org.springblade.modules.common.appversion.pojo.vo.AppVersionVO;
import org.springblade.modules.common.appversion.service.IAppVersionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * app api for the UI of device
 *
 */
@RestController
@AllArgsConstructor
@RequestMapping("deviceApi/charger/v1.0")
public class ChargerApiController extends BladeController {

	private final IAppVersionService appVersionService;



	@GetMapping("/getToken")
	public R<IPage<AppVersionVO>> getToken(@RequestParam Map<String, Object> appVersion) {
//		IPage<AppVersionEntity> pages = appVersionService.page(Condition.getPage(query), Condition.getQueryWrapper(appVersion, AppVersionEntity.class));
//		return R.data(AppVersionWrapper.build().pageVO(pages));
		return R.status(true);
	}

	@GetMapping("/latestApplet")
	public R<AppVersionEntity> latestApplet(@RequestParam String type) {
		Map<String, Object> sqlMap = new HashMap<>();
		sqlMap.put("type",type);
		sqlMap.put("status", 1);
		AppVersionEntity appVersionEntity = appVersionService.getOne(Condition.getQueryWrapper(sqlMap, AppVersionEntity.class));
		return R.data(appVersionEntity);
	}

}
