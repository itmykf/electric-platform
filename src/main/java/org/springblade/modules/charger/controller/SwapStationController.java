package org.springblade.modules.charger.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.charger.excel.SwapStationExcel;
import org.springblade.modules.charger.pojo.entity.SwapStationEntity;
import org.springblade.modules.charger.pojo.vo.SwapStationVO;
import org.springblade.modules.charger.service.ISwapStationService;
import org.springblade.modules.charger.wrapper.SwapStationWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 换电柜 控制器
 *
 * @author ludada
 * @since 2024-09-29
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-swapstation/swapStation")
@Tag(name = "换电柜", description = "换电柜接口")
public class SwapStationController extends BladeController {

	private final ISwapStationService swapStationService;

	/**
	 * 换电柜 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入swapStation")
	public R<SwapStationVO> detail(SwapStationEntity swapStation) {
		SwapStationEntity detail = swapStationService.getOne(Condition.getQueryWrapper(swapStation));
		return R.data(SwapStationWrapper.build().entityVO(detail));
	}

	/**
	 * 换电柜 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入swapStation")
	public R<IPage<SwapStationVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> swapStation, Query query) {
		IPage<SwapStationEntity> pages = swapStationService.page(Condition.getPage(query), Condition.getQueryWrapper(swapStation, SwapStationEntity.class));
		return R.data(SwapStationWrapper.build().pageVO(pages));
	}

	/**
	 * 换电柜 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@Operation(summary = "分页", description = "传入swapStation")
	public R<IPage<SwapStationVO>> page(SwapStationVO swapStation, Query query) {
		IPage<SwapStationVO> pages = swapStationService.selectSwapStationPage(Condition.getPage(query), swapStation);
		return R.data(pages);
	}

	/**
	 * 换电柜 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@Operation(summary = "新增", description = "传入swapStation")
	public R save(@Valid @RequestBody SwapStationEntity swapStation) {
		return R.status(swapStationService.save(swapStation));
	}

	/**
	 * 换电柜 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@Operation(summary = "修改", description = "传入swapStation")
	public R update(@Valid @RequestBody SwapStationEntity swapStation) {
		return R.status(swapStationService.updateById(swapStation));
	}

	/**
	 * 换电柜 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入swapStation")
	public R submit(@Valid @RequestBody SwapStationEntity swapStation) {
		return R.status(swapStationService.saveOrUpdate(swapStation));
	}

	/**
	 * 换电柜 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(swapStationService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-swapStation")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入swapStation")
	public void exportSwapStation(@Parameter(hidden = true) @RequestParam Map<String, Object> swapStation, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<SwapStationEntity> queryWrapper = Condition.getQueryWrapper(swapStation, SwapStationEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(SwapStation::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(SwapStationEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<SwapStationExcel> list = swapStationService.exportSwapStation(queryWrapper);
		ExcelUtil.export(response, "换电柜数据" + DateUtil.time(), "换电柜数据表", list, SwapStationExcel.class);
	}

}
