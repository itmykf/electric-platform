package org.springblade.modules.charger.controller;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.charger.excel.ChargerExcel;
import org.springblade.modules.charger.pojo.entity.ChargerEntity;
import org.springblade.modules.charger.pojo.req.StartingChargeRequestBody;
import org.springblade.modules.charger.pojo.req.UpdateFirmwareRequestBody;
import org.springblade.modules.charger.pojo.vo.ChargerVO;
import org.springblade.modules.charger.pojo.vo.Inside.*;
import org.springblade.modules.charger.service.IChargerService;
import org.springblade.modules.charger.wrapper.ChargerWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 充电桩管理 控制器
 *
 * @author lhb
 * @since 2024-10-04
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-charger/charger")
@Tag(name = "充电桩管理", description = "充电桩管理接口")
public class ChargerController extends BladeController {

	private final IChargerService chargerService;

	/**
	 * 充电桩管理 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入charger")
	public R<ChargerVO> detail(ChargerEntity charger) {
		ChargerEntity detail = chargerService.getOne(Condition.getQueryWrapper(charger));
		return R.data(ChargerWrapper.build().entityVO(detail));
	}

	/**
	 * 充电桩管理 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入charger")
	public R<IPage<ChargerVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> charger, Query query) {
		IPage<ChargerEntity> pages = chargerService.page(Condition.getPage(query), Condition.getQueryWrapper(charger, ChargerEntity.class));
		IPage<ChargerVO> page = ChargerWrapper.build().pageVO(pages);
		for (ChargerVO record : page.getRecords()) {
			if (StringUtils.isNotBlank(record.getChargingOrder())) {
				record.setChargingOrderJson(JSONObject.parseObject(record.getChargingOrder(), ChargingOrderJsonVO.class));
			}
			if (StringUtils.isNotBlank(record.getCurrent())) {
				record.setCurrentJson(JSONObject.parseObject(record.getCurrent(), CurrentJsonVO.class));
			}
			if (StringUtils.isNotBlank(record.getVoltage())) {
				record.setVoltageJson(JSONObject.parseObject(record.getVoltage(), VoltageJsonVO.class));
			}
			if (StringUtils.isNotBlank(record.getPower())) {
				record.setPowerJson(JSONObject.parseObject(record.getPower(), PowerJsonVO.class));
			}
			if (StringUtils.isNotBlank(record.getStatusJson())) {
				record.setStatusJsonJson(JSONObject.parseObject(record.getStatusJson(), StatusJsonVO.class));
			}
		}
		return R.data(page);
	}

	/**
	 * 充电桩管理 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入charger")
	public R submit(@Valid @RequestBody ChargerEntity charger) {
		charger.setStatus(1);
		charger.setCurrent(JSONObject.toJSONString(new CurrentJsonVO()));
		charger.setVoltage(JSONObject.toJSONString(new VoltageJsonVO()));
		charger.setPower(JSONObject.toJSONString(new PowerJsonVO()));
		charger.setChargingOrder(JSONObject.toJSONString(new ChargingOrderJsonVO()));
		charger.setStatusJson(JSONObject.toJSONString(new StatusJsonVO()));
		return R.status(chargerService.saveOrUpdate(charger));
	}

	/**
	 * 充电桩管理 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(chargerService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-charger")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入charger")
	public void exportCharger(@Parameter(hidden = true) @RequestParam Map<String, Object> charger, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<ChargerEntity> queryWrapper = Condition.getQueryWrapper(charger, ChargerEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(Charger::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(ChargerEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<ChargerExcel> list = chargerService.exportCharger(queryWrapper);
		ExcelUtil.export(response, "充电桩管理数据" + DateUtil.time(), "充电桩管理数据表", list, ChargerExcel.class);
	}

	@PostMapping("/startCharge")
	@ApiOperationSupport(order = 10)
	@Operation(summary = "启动充电", description = "启动充电")
	public R startCharge(@RequestBody StartingChargeRequestBody requestBody) {
		return R.status(chargerService.startCharge(requestBody));
	}

	// 停止充电
	@PostMapping("/stopCharge")
	@ApiOperationSupport(order = 11)
	@Operation(summary = "停止充电", description = "停止充电")
	public R stopCharge(@RequestBody StartingChargeRequestBody requestBody) {
		return R.status(chargerService.stopCharge(requestBody));
	}

	// ota升级
	@PostMapping("/updateFirmware")
	@ApiOperationSupport(order = 12)
	@Operation(summary = "ota升级", description = "ota升级")
	public R updateFirmware(@RequestBody UpdateFirmwareRequestBody requestBody) {
		return R.status(chargerService.updateFirmware(requestBody));
	}
}
