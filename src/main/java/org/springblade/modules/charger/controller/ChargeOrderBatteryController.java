/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.modules.charger.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import jakarta.validation.Valid;

import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.modules.charger.pojo.entity.ChargeOrderBatteryEntity;
import org.springblade.modules.charger.pojo.vo.ChargeOrderBatteryVO;
import org.springblade.modules.charger.excel.ChargeOrderBatteryExcel;
import org.springblade.modules.charger.wrapper.ChargeOrderBatteryWrapper;
import org.springblade.modules.charger.service.IChargeOrderBatteryService;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import java.util.Map;
import java.util.List;
import jakarta.servlet.http.HttpServletResponse;

/**
 * 订单电池信息 控制器
 *
 * @author lhb
 * @since 2024-10-05
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-chargeorderbattery/chargeOrderBattery")
@Tag(name = "订单电池信息", description = "订单电池信息接口")
public class ChargeOrderBatteryController extends BladeController {

	private final IChargeOrderBatteryService chargeOrderBatteryService;

	/**
	 * 订单电池信息 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description  = "传入chargeOrderBattery")
	public R<ChargeOrderBatteryVO> detail(ChargeOrderBatteryEntity chargeOrderBattery) {
		ChargeOrderBatteryEntity detail = chargeOrderBatteryService.getOne(Condition.getQueryWrapper(chargeOrderBattery));
		return R.data(ChargeOrderBatteryWrapper.build().entityVO(detail));
	}
	/**
	 * 订单电池信息 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description  = "传入chargeOrderBattery")
	public R<IPage<ChargeOrderBatteryVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> chargeOrderBattery, Query query) {
		IPage<ChargeOrderBatteryEntity> pages = chargeOrderBatteryService.page(Condition.getPage(query), Condition.getQueryWrapper(chargeOrderBattery, ChargeOrderBatteryEntity.class));
		return R.data(ChargeOrderBatteryWrapper.build().pageVO(pages));
	}

	/**
	 * 订单电池信息 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@Operation(summary = "分页", description  = "传入chargeOrderBattery")
	public R<IPage<ChargeOrderBatteryVO>> page(ChargeOrderBatteryVO chargeOrderBattery, Query query) {
		IPage<ChargeOrderBatteryVO> pages = chargeOrderBatteryService.selectChargeOrderBatteryPage(Condition.getPage(query), chargeOrderBattery);
		return R.data(pages);
	}

	/**
	 * 订单电池信息 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@Operation(summary = "新增", description  = "传入chargeOrderBattery")
	public R save(@Valid @RequestBody ChargeOrderBatteryEntity chargeOrderBattery) {
		return R.status(chargeOrderBatteryService.save(chargeOrderBattery));
	}

	/**
	 * 订单电池信息 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@Operation(summary = "修改", description  = "传入chargeOrderBattery")
	public R update(@Valid @RequestBody ChargeOrderBatteryEntity chargeOrderBattery) {
		return R.status(chargeOrderBatteryService.updateById(chargeOrderBattery));
	}

	/**
	 * 订单电池信息 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description  = "传入chargeOrderBattery")
	public R submit(@Valid @RequestBody ChargeOrderBatteryEntity chargeOrderBattery) {
		return R.status(chargeOrderBatteryService.saveOrUpdate(chargeOrderBattery));
	}

	/**
	 * 订单电池信息 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description  = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(chargeOrderBatteryService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-chargeOrderBattery")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description  = "传入chargeOrderBattery")
	public void exportChargeOrderBattery(@Parameter(hidden = true) @RequestParam Map<String, Object> chargeOrderBattery, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<ChargeOrderBatteryEntity> queryWrapper = Condition.getQueryWrapper(chargeOrderBattery, ChargeOrderBatteryEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(ChargeOrderBattery::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(ChargeOrderBatteryEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<ChargeOrderBatteryExcel> list = chargeOrderBatteryService.exportChargeOrderBattery(queryWrapper);
		ExcelUtil.export(response, "订单电池信息数据" + DateUtil.time(), "订单电池信息数据表", list, ChargeOrderBatteryExcel.class);
	}

}
