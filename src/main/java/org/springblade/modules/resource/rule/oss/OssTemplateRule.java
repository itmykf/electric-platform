package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.oss.OssTemplate;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.resource.rule.context.OssContext;

/**
 * OSS接口读取校验
 *
 * @author Chill
 */
@LiteflowComponent(id = "ossTemplateRule", name = "OSS接口读取校验")
public class OssTemplateRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		OssContext contextBean = this.getContextBean(OssContext.class);
		OssTemplate ossTemplate = contextBean.getOssTemplate();

		if (Func.isEmpty(ossTemplate)) {
			throw new ServiceException("OSS接口读取失败!");
		}
	}
}
