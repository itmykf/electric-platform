package org.springblade.modules.resource.rule.context;

import lombok.Data;
import org.springblade.core.redis.cache.BladeRedis;
import org.springblade.core.sms.SmsTemplate;
import org.springblade.core.sms.props.SmsProperties;
import org.springblade.modules.resource.pojo.entity.Sms;

import java.util.Map;

/**
 * Sms上下文
 *
 * @author Chill
 */
@Data
public class SmsContext {
	/**
	 * 是否有缓存
	 */
	private Boolean isCached;
	/**
	 * sms数据
	 */
	private Sms sms;
	/**
	 * sms接口
	 */
	private SmsTemplate smsTemplate;
	/**
	 * sms配置
	 */
	private SmsProperties smsProperties;
	/**
	 * redis工具
	 */
	private BladeRedis bladeRedis;
	/**
	 * SmsTemplate配置缓存池
	 */
	private Map<String, SmsTemplate> templatePool;
	/**
	 * sms配置缓存池
	 */
	private Map<String, Sms> smsPool;


}
