package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.oss.OssTemplate;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.resource.pojo.entity.Oss;
import org.springblade.modules.resource.rule.context.OssContext;

import java.util.Map;

/**
 * Oss后置处理
 *
 * @author Chill
 */
@LiteflowComponent(id = "finallyOssRule", name = "OSS构建后置处理")
public class FinallyOssRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		String tenantId = this.getRequestData();
		OssContext contextBean = this.getContextBean(OssContext.class);
		Map<String, Oss> ossPool = contextBean.getOssPool();
		Map<String, OssTemplate> templatePool = contextBean.getTemplatePool();

		if (contextBean.getIsCached()) {
			OssTemplate template = templatePool.get(tenantId);
			contextBean.setOssTemplate(template);
		} else {
			Oss oss = contextBean.getOss();
			OssTemplate template = contextBean.getOssTemplate();
			if (Func.hasEmpty(template, oss)) {
				throw new ServiceException("OSS接口读取失败!");
			} else {
				templatePool.put(tenantId, template);
				ossPool.put(tenantId, oss);
			}
		}

	}
}
