package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;
import org.springblade.modules.resource.rule.context.OssContext;

/**
 * Oss缓存判断
 *
 * @author Chill
 */
@LiteflowComponent(id = "ossCacheRule", name = "OSS缓存判断")
public class OssCacheRule extends NodeSwitchComponent {

	@Override
	public String processSwitch() {
		OssContext contextBean = this.getContextBean(OssContext.class);
		// 若判断配置已缓存则直接读取，否则进入下一步构建新数据
		if (contextBean.getIsCached()) {
			return "ossReadRule";
		} else {
			return "ossNewRule";
		}
	}

}
