package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.oss.props.OssProperties;
import org.springblade.core.oss.rule.BladeOssRule;
import org.springblade.modules.resource.pojo.entity.Oss;
import org.springblade.modules.resource.rule.context.OssContext;

/**
 * OSS数据创建
 *
 * @author Chill
 */
@LiteflowComponent(id = "ossDataRule", name = "OSS数据创建")
public class OssDataRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		OssContext contextBean = this.getContextBean(OssContext.class);
		Oss oss = contextBean.getOss();
		OssProperties ossProperties = contextBean.getOssProperties();
		// 若采用默认设置则开启多租户模式, 若是用户自定义oss则不开启
		if (oss.getEndpoint().equals(ossProperties.getEndpoint()) && oss.getAccessKey().equals(ossProperties.getAccessKey()) && ossProperties.getTenantMode()) {
			contextBean.setOssRule(new BladeOssRule(Boolean.TRUE));
		} else {
			contextBean.setOssRule(new BladeOssRule(Boolean.FALSE));
		}
	}
}
