package org.springblade.modules.resource.rule.sms;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.sms.enums.SmsEnum;
import org.springblade.modules.resource.pojo.entity.Sms;
import org.springblade.modules.resource.rule.context.SmsContext;

/**
 * Sms构建判断
 *
 * @author Chill
 */
@LiteflowComponent(id = "smsBuildRule", name = "SMS构建条件判断")
public class SmsBuildRule extends NodeSwitchComponent {

	@Override
	public String processSwitch() {
		SmsContext contextBean = this.getContextBean(SmsContext.class);
		Sms sms = contextBean.getSms();

		if (contextBean.getIsCached()) {
			return "cacheSmsRule";
		} else if (sms.getCategory() == SmsEnum.YUNPIAN.getCategory()) {
			return "yunpianSmsRule";
		} else if (sms.getCategory() == SmsEnum.QINIU.getCategory()) {
			return "qiniuSmsRule";
		} else if (sms.getCategory() == SmsEnum.ALI.getCategory()) {
			return "aliSmsRule";
		} else if (sms.getCategory() == SmsEnum.TENCENT.getCategory()) {
			return "tencentSmsRule";
		}

		throw new ServiceException("未找到SMS配置");
	}
}
