package org.springblade.modules.resource.rule.sms;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.sms.SmsTemplate;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.resource.pojo.entity.Sms;
import org.springblade.modules.resource.rule.context.SmsContext;

import java.util.Map;

/**
 * 缓存短信构建类
 *
 * @author Chill
 */
@LiteflowComponent(id = "cacheSmsRule", name = "缓存SMS构建")
public class CacheSmsRule extends NodeComponent {

	@Override
	public void process() throws Exception {
		// 获取上下文
		String tenantId = this.getRequestData();
		SmsContext contextBean = this.getContextBean(SmsContext.class);

		Map<String, Sms> smsPool = contextBean.getSmsPool();
		Map<String, SmsTemplate> templatePool = contextBean.getTemplatePool();
		Sms smsCached = smsPool.get(tenantId);
		SmsTemplate template = templatePool.get(tenantId);
		if (Func.hasEmpty(template, smsCached)) {
			throw new ServiceException("SMS缓存读取失败!");
		}

	}
}
