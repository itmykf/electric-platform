package org.springblade.modules.resource.rule.context;

import lombok.Data;
import org.springblade.core.oss.OssTemplate;
import org.springblade.core.oss.props.OssProperties;
import org.springblade.core.oss.rule.OssRule;
import org.springblade.modules.resource.pojo.entity.Oss;

import java.util.Map;

/**
 * Oss上下文
 *
 * @author Chill
 */
@Data
public class OssContext {
	/**
	 * 是否有缓存
	 */
	private Boolean isCached;
	/**
	 * oss数据
	 */
	private Oss oss;
	/**
	 * oss规则
	 */
	private OssRule ossRule;
	/**
	 * oss接口
	 */
	private OssTemplate ossTemplate;
	/**
	 * oss配置
	 */
	private OssProperties ossProperties;
	/**
	 * OssTemplate配置缓存池
	 */
	private Map<String, OssTemplate> templatePool;
	/**
	 * oss配置缓存池
	 */
	private Map<String, Oss> ossPool;


}
