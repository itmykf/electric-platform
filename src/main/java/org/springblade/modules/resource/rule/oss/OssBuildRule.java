package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.oss.enums.OssEnum;
import org.springblade.modules.resource.pojo.entity.Oss;
import org.springblade.modules.resource.rule.context.OssContext;

/**
 * Oss构建判断
 *
 * @author Chill
 */
@LiteflowComponent(id = "ossBuildRule", name = "OSS构建条件判断")
public class OssBuildRule extends NodeSwitchComponent {

	@Override
	public String processSwitch() {
		OssContext contextBean = this.getContextBean(OssContext.class);
		Oss oss = contextBean.getOss();
		if (oss.getCategory() == OssEnum.MINIO.getCategory()) {
			return "minioRule";
		} else if (oss.getCategory() == OssEnum.QINIU.getCategory()) {
			return "qiniuOssRule";
		} else if (oss.getCategory() == OssEnum.ALI.getCategory()) {
			return "aliOssRule";
		} else if (oss.getCategory() == OssEnum.TENCENT.getCategory()) {
			return "tencentCosRule";
		} else if (oss.getCategory() == OssEnum.HUAWEI.getCategory()) {
			return "huaweiObsRule";
		} else if (oss.getCategory() == OssEnum.AMAZONS3.getCategory()) {
			return "amazonS3Rule";
		}
		throw new ServiceException("未找到OSS配置");
	}
}
