package org.springblade.modules.resource.rule.oss;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import org.springblade.core.oss.OssTemplate;
import org.springblade.core.tool.utils.Func;
import org.springblade.modules.resource.pojo.entity.Oss;
import org.springblade.modules.resource.rule.context.OssContext;

import java.util.Map;

/**
 * Oss前置处理
 *
 * @author Chill
 */
@LiteflowComponent(id = "preOssRule", name = "OSS构建前置处理")
public class PreOssRule extends NodeComponent {
	@Override
	public void process() throws Exception {
		String tenantId = this.getRequestData();
		OssContext contextBean = this.getContextBean(OssContext.class);
		Map<String, Oss> ossPool = contextBean.getOssPool();
		Map<String, OssTemplate> templatePool = contextBean.getTemplatePool();
		Oss oss = contextBean.getOss();
		Oss ossCached = ossPool.get(tenantId);
		OssTemplate template = templatePool.get(tenantId);
		// 若为空或者不一致，则重新加载
		if (Func.hasEmpty(template, ossCached)
			|| !Func.equalsSafe(oss.getEndpoint(), ossCached.getEndpoint())
			|| !Func.equalsSafe(oss.getTransformEndpoint(), ossCached.getTransformEndpoint())
			|| !Func.equalsSafe(oss.getAccessKey(), ossCached.getAccessKey())) {
			contextBean.setIsCached(Boolean.FALSE);
		} else {
			contextBean.setIsCached(Boolean.TRUE);
		}
	}

}
