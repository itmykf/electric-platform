package org.springblade.modules.resource.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 实体类
 *
 * @author BladeX
 */
@Data
@TableName("blade_oss")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "Oss对象")
public class Oss extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 所属分类
	 */
	@Schema(description = "所属分类")
	private Integer category;

	/**
	 * 资源编号
	 */
	@Schema(description = "资源编号")
	private String ossCode;

	/**
	 * 资源地址
	 */
	@Schema(description = "资源地址")
	private String endpoint;

	/**
	 * 外网资源地址
	 */
	@Schema(description = "外网资源地址")
	private String transformEndpoint;
	/**
	 * accessKey
	 */
	@Schema(description = "accessKey")
	private String accessKey;
	/**
	 * secretKey
	 */
	@Schema(description = "secretKey")
	private String secretKey;
	/**
	 * 空间名
	 */
	@Schema(description = "空间名")
	private String bucketName;
	/**
	 * 应用ID TencentCOS需要
	 */
	@Schema(description = "应用ID")
	private String appId;
	/**
	 * 地域简称 TencentCOS需要
	 */
	@Schema(description = "地域简称")
	private String region;
	/**
	 * 备注
	 */
	@Schema(description = "备注")
	private String remark;


}
