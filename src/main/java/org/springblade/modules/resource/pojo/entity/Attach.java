package org.springblade.modules.resource.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 附件表实体类
 *
 * @author Chill
 */
@Data
@TableName("blade_attach")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "附件表")
public class Attach extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 附件地址
	 */
	@Schema(description = "附件地址")
	private String link;
	/**
	 * 附件域名
	 */
	@Schema(description = "附件域名")
	private String domainUrl;
	/**
	 * 附件名称
	 */
	@Schema(description = "附件名称")
	private String name;
	/**
	 * 附件原名
	 */
	@Schema(description = "附件原名")
	private String originalName;
	/**
	 * 附件拓展名
	 */
	@Schema(description = "附件拓展名")
	private String extension;
	/**
	 * 附件大小
	 */
	@Schema(description = "附件大小")
	private Long attachSize;


}
