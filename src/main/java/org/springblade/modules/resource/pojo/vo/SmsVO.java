package org.springblade.modules.resource.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.resource.pojo.entity.Sms;

import java.io.Serial;

/**
 * 短信配置表视图实体类
 *
 * @author BladeX
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "短信配置表")
public class SmsVO extends Sms {
	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 分类名
	 */
	private String categoryName;

	/**
	 * 是否启用
	 */
	private String statusName;

}
