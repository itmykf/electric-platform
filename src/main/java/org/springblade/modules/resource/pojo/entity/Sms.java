package org.springblade.modules.resource.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 短信配置表实体类
 *
 * @author BladeX
 */
@Data
@TableName("blade_sms")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "短信配置表")
public class Sms extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 资源编号
	 */
	@Schema(description = "资源编号")
	private String smsCode;

	/**
	 * 模板ID
	 */
	@Schema(description = "模板ID")
	private String templateId;
	/**
	 * 分类
	 */
	@Schema(description = "分类")
	private Integer category;
	/**
	 * accessKey
	 */
	@Schema(description = "accessKey")
	private String accessKey;
	/**
	 * secretKey
	 */
	@Schema(description = "secretKey")
	private String secretKey;
	/**
	 * regionId
	 */
	@Schema(description = "regionId")
	private String regionId;
	/**
	 * 短信签名
	 */
	@Schema(description = "短信签名")
	private String signName;
	/**
	 * 备注
	 */
	@Schema(description = "备注")
	private String remark;


}
