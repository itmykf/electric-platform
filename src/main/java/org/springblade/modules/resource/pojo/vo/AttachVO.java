package org.springblade.modules.resource.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.resource.pojo.entity.Attach;

import java.io.Serial;

/**
 * 附件表视图实体类
 *
 * @author Chill
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "附件表VO")
public class AttachVO extends Attach {
	@Serial
	private static final long serialVersionUID = 1L;

}
