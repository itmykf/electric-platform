package org.springblade.modules.resource.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.modules.resource.mapper.AttachMapper;
import org.springblade.modules.resource.pojo.entity.Attach;
import org.springblade.modules.resource.pojo.vo.AttachVO;
import org.springblade.modules.resource.service.IAttachService;
import org.springframework.stereotype.Service;

/**
 * 附件表 服务实现类
 *
 * @author Chill
 */
@Service
public class AttachServiceImpl extends BaseServiceImpl<AttachMapper, Attach> implements IAttachService {

	@Override
	public IPage<AttachVO> selectAttachPage(IPage<AttachVO> page, AttachVO attach) {
		return page.setRecords(baseMapper.selectAttachPage(page, attach));
	}

}
