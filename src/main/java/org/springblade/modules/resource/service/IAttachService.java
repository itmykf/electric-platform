package org.springblade.modules.resource.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.resource.pojo.entity.Attach;
import org.springblade.modules.resource.pojo.vo.AttachVO;

/**
 * 附件表 服务类
 *
 * @author Chill
 */
public interface IAttachService extends BaseService<Attach> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param attach
	 * @return
	 */
	IPage<AttachVO> selectAttachPage(IPage<AttachVO> page, AttachVO attach);

}
