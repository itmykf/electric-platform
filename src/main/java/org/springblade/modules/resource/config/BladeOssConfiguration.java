package org.springblade.modules.resource.config;

import com.yomahub.liteflow.core.FlowExecutor;
import lombok.AllArgsConstructor;
import org.springblade.core.oss.props.OssProperties;
import org.springblade.modules.resource.builder.OssBuilder;
import org.springblade.modules.resource.service.IOssService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Oss配置类
 *
 * @author Chill
 */
@Configuration(proxyBeanMethods = false)
@AllArgsConstructor
public class BladeOssConfiguration {

	private final OssProperties ossProperties;

	private final IOssService ossService;

	private final FlowExecutor flowExecutor;

	@Bean
	public OssBuilder ossBuilder() {
		return new OssBuilder(ossProperties, ossService, flowExecutor);
	}

}
