package org.springblade.modules.develop.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.modules.develop.pojo.entity.Model;
import org.springblade.modules.develop.pojo.entity.ModelPrototype;

import java.io.Serial;
import java.util.List;

/**
 * 代码模型DTO
 *
 * @author Chill
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ModelDTO extends Model {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 代码建模原型
	 */
	private List<ModelPrototype> prototypes;

}
