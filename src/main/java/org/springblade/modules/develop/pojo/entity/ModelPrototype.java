package org.springblade.modules.develop.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.mp.base.BaseEntity;

import java.io.Serial;

/**
 * 数据原型表实体类
 *
 * @author Chill
 */
@Data
@TableName("blade_model_prototype")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "数据原型表")
public class ModelPrototype extends BaseEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 模型主键
	 */
	@Schema(description = "模型主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long modelId;
	/**
	 * 物理列名
	 */
	@Schema(description = "物理列名")
	private String jdbcName;
	/**
	 * 物理类型
	 */
	@Schema(description = "物理类型")
	private String jdbcType;
	/**
	 * 注释说明
	 */
	@Schema(description = "注释说明")
	private String jdbcComment;
	/**
	 * 实体列名
	 */
	@Schema(description = "实体列名")
	private String propertyName;
	/**
	 * 实体类型
	 */
	@Schema(description = "实体类型")
	private String propertyType;
	/**
	 * 实体类型引用
	 */
	@Schema(description = "实体类型引用")
	private String propertyEntity;
	/**
	 * 列表显示
	 */
	@Schema(description = "列表显示")
	private Integer isList;
	/**
	 * 表单显示
	 */
	@Schema(description = "表单显示")
	private Integer isForm;
	/**
	 * 独占一行
	 */
	@Schema(description = "独占一行")
	private Integer isRow;
	/**
	 * 组件类型
	 */
	@Schema(description = "组件类型")
	private String componentType;
	/**
	 * 字典编码
	 */
	@Schema(description = "字典编码")
	private String dictCode;
	/**
	 * 是否必填
	 */
	@Schema(description = "是否必填")
	private Integer isRequired;
	/**
	 * 查询配置
	 */
	@Schema(description = "查询配置")
	private Integer isQuery;
	/**
	 * 查询类型
	 */
	@Schema(description = "查询类型")
	private String queryType;


}
