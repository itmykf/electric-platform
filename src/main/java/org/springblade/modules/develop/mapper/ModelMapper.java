package org.springblade.modules.develop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.develop.pojo.entity.Model;

/**
 * 数据模型表 Mapper 接口
 *
 * @author Chill
 */
public interface ModelMapper extends BaseMapper<Model> {

}
