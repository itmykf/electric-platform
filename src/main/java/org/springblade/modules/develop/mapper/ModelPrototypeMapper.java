package org.springblade.modules.develop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.develop.pojo.entity.ModelPrototype;

/**
 * 数据原型表 Mapper 接口
 *
 * @author Chill
 */
public interface ModelPrototypeMapper extends BaseMapper<ModelPrototype> {

}
