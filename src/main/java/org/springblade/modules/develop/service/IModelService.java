package org.springblade.modules.develop.service;

import org.springblade.core.mp.base.BaseService;
import org.springblade.modules.develop.pojo.entity.Model;

import java.util.List;

/**
 * 数据模型表 服务类
 *
 * @author Chill
 */
public interface IModelService extends BaseService<Model> {

	/**
	 * 删除模型
	 *
	 * @param ids 主键集合
	 * @return boolean
	 */
	boolean delete(List<Long> ids);

}
