package org.springblade.modules.auth.granter;

import jakarta.servlet.http.HttpServletRequest;
import org.springblade.core.oauth2.constant.OAuth2TokenConstant;
import org.springblade.core.oauth2.exception.UserInvalidException;
import org.springblade.core.oauth2.granter.AbstractTokenGranter;
import org.springblade.core.oauth2.handler.PasswordHandler;
import org.springblade.core.oauth2.props.OAuth2Properties;
import org.springblade.core.oauth2.provider.OAuth2Request;
import org.springblade.core.oauth2.service.OAuth2ClientService;
import org.springblade.core.oauth2.service.OAuth2User;
import org.springblade.core.oauth2.service.OAuth2UserService;
import org.springblade.core.sms.model.SmsCode;
import org.springblade.core.tool.utils.SM2Util;
import org.springblade.core.tool.utils.StringPool;
import org.springblade.core.tool.utils.StringUtil;
import org.springblade.core.tool.utils.WebUtil;
import org.springblade.modules.resource.builder.SmsBuilder;
import org.springframework.stereotype.Component;

/**
 * SmsTokenGranter
 *
 * @author BladeX
 */
@Component
public class SmsTokenGranter extends AbstractTokenGranter {

	private final OAuth2UserService userService;
	private final SmsBuilder smsBuilder;
	private final OAuth2Properties properties;

	public SmsTokenGranter(OAuth2ClientService clientService, OAuth2UserService userService, PasswordHandler passwordHandler, SmsBuilder smsBuilder, OAuth2Properties properties) {
		super(clientService, userService, passwordHandler);
		this.userService = userService;
		this.smsBuilder = smsBuilder;
		this.properties = properties;
	}

	@Override
	public String type() {
		return SMS_CODE;
	}

	@Override
	public OAuth2User user(OAuth2Request request) {
		// 获取基础信息
		String tenantId = request.getTenantId();
		SmsCode smsCode = buildSmsCode();
		// 校验手机加密认证
		String decryptedPhone = SM2Util.decrypt(smsCode.getPhone(), properties.getPublicKey(), properties.getPrivateKey());
		if (StringUtil.isBlank(decryptedPhone)) {
			throw new UserInvalidException(OAuth2TokenConstant.USER_PHONE_NOT_FOUND);
		}
		// 获取短信验证信息
		boolean temp = smsBuilder.template(tenantId, StringPool.EMPTY).validateMessage(smsCode.setPhone(decryptedPhone));
		if (!temp) {
			throw new UserInvalidException(OAuth2TokenConstant.CAPTCHA_NOT_CORRECT);
		}
		// 获取用户信息
		OAuth2User user = userService.loadByPhone(decryptedPhone, request);
		// 校验用户信息
		if (!userService.validateUser(user)) {
			throw new UserInvalidException(OAuth2TokenConstant.TOKEN_NOT_CORRECT);
		}
		// 设置客户端信息
		user.setClient(client(request));
		return user;
	}

	private SmsCode buildSmsCode() {
		HttpServletRequest request = WebUtil.getRequest();
		return new SmsCode().setId(request.getParameter("id"))
			.setPhone(request.getParameter("phone"))
			.setValue(request.getParameter("value"));
	}
}
