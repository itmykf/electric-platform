package org.springblade.modules.auth.endpoint;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springblade.core.oauth2.props.OAuth2Properties;
import org.springblade.core.oauth2.provider.OAuth2Request;
import org.springblade.core.oauth2.service.OAuth2User;
import org.springblade.core.oauth2.service.OAuth2UserService;
import org.springblade.core.sms.model.SmsCode;
import org.springblade.core.sms.model.SmsData;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.SM2Util;
import org.springblade.core.tool.utils.StringPool;
import org.springblade.core.tool.utils.StringUtil;
import org.springblade.modules.resource.builder.SmsBuilder;
import org.springblade.modules.resource.utils.SmsUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static org.springblade.core.oauth2.constant.OAuth2TokenConstant.USER_PHONE_NOT_FOUND;
import static org.springblade.modules.resource.utils.SmsUtil.*;

/**
 * Oauth2SmsEndpoint
 *
 * @author Chill
 */
@RestController
@AllArgsConstructor
@Tag(name = "用户短信认证", description = "4 - OAuth2短信认证端点")
public class Oauth2SmsEndpoint {

	/**
	 * 短信服务构建类
	 */
	private final SmsBuilder smsBuilder;

	/**
	 * 用户服务类
	 */
	private final OAuth2UserService userService;

	/**
	 * OAuth2配置类
	 */
	private final OAuth2Properties properties;

	/**
	 * 短信验证码发送
	 *
	 * @param tenantId 租户ID
	 * @param phone    手机号
	 */
	@SneakyThrows
	@PostMapping("/oauth/sms/send-validate")
	public R sendValidate(@RequestParam String tenantId, @RequestParam String phone) {
		// 校验手机加密认证，防止恶意发送验证码
		String decryptedPhone = SM2Util.decrypt(phone, properties.getPublicKey(), properties.getPrivateKey());
		if (StringUtil.isBlank(decryptedPhone)) {
			return R.fail(USER_PHONE_NOT_FOUND);
		}
		// 校验手机是否已注册，防止恶意发送验证码
		OAuth2Request request = OAuth2Request.create();
		request.setTenantId(tenantId);
		OAuth2User oAuth2User = userService.loadByPhone(decryptedPhone, request);
		if (oAuth2User == null) {
			return R.fail(USER_PHONE_NOT_FOUND);
		}
		// 用户存在则发送验证码
		Map<String, String> params = SmsUtil.getValidateParams();
		SmsCode smsCode = smsBuilder.template(tenantId, StringPool.EMPTY).sendValidate(new SmsData(params).setKey(PARAM_KEY), decryptedPhone);
		return smsCode.isSuccess() ? R.data(smsCode, SEND_SUCCESS) : R.fail(SEND_FAIL);
	}

}
