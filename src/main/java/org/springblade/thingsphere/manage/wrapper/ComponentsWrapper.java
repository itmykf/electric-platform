package org.springblade.thingsphere.manage.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;

import java.util.Objects;

/**
 * 网络组件 包装类,返回视图层所需的字段
 *
 * @author lhb
 * @since 2024-09-27
 */
public class ComponentsWrapper extends BaseEntityWrapper<ComponentsEntity, ComponentsVO> {

	public static ComponentsWrapper build() {
		return new ComponentsWrapper();
	}

	@Override
	public ComponentsVO entityVO(ComponentsEntity components) {
		ComponentsVO componentsVO = Objects.requireNonNull(BeanUtil.copyProperties(components, ComponentsVO.class));

		//User createUser = UserCache.getUser(components.getCreateUser());
		//User updateUser = UserCache.getUser(components.getUpdateUser());
		//componentsVO.setCreateUserName(createUser.getName());
		//componentsVO.setUpdateUserName(updateUser.getName());

		return componentsVO;
	}


}
