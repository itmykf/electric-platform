package org.springblade.thingsphere.manage.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;
import org.springblade.thingsphere.manage.pojo.vo.GatewayVO;

import java.util.Objects;

/**
 * 设备网关 包装类,返回视图层所需的字段
 *
 * @author lhb
 * @since 2024-09-30
 */
public class GatewayWrapper extends BaseEntityWrapper<GatewayEntity, GatewayVO> {

	public static GatewayWrapper build() {
		return new GatewayWrapper();
	}

	@Override
	public GatewayVO entityVO(GatewayEntity gateway) {
		GatewayVO gatewayVO = Objects.requireNonNull(BeanUtil.copyProperties(gateway, GatewayVO.class));

		//User createUser = UserCache.getUser(gateway.getCreateUser());
		//User updateUser = UserCache.getUser(gateway.getUpdateUser());
		//gatewayVO.setCreateUserName(createUser.getName());
		//gatewayVO.setUpdateUserName(updateUser.getName());

		return gatewayVO;
	}


}
