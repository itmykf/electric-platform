package org.springblade.thingsphere.manage.wrapper;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.thingsphere.manage.pojo.entity.ProtocolEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProtocolVO;

import java.util.Objects;

/**
 * 协议管理 包装类,返回视图层所需的字段
 *
 * @author lhb
 * @since 2024-09-26
 */
public class ProtocolWrapper extends BaseEntityWrapper<ProtocolEntity, ProtocolVO> {

	public static ProtocolWrapper build() {
		return new ProtocolWrapper();
	}

	@Override
	public ProtocolVO entityVO(ProtocolEntity protocol) {
		ProtocolVO protocolVO = Objects.requireNonNull(BeanUtil.copyProperties(protocol, ProtocolVO.class));

		//User createUser = UserCache.getUser(protocol.getCreateUser());
		//User updateUser = UserCache.getUser(protocol.getUpdateUser());
		//protocolVO.setCreateUserName(createUser.getName());
		//protocolVO.setUpdateUserName(updateUser.getName());

		return protocolVO;
	}


}
