/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.thingsphere.manage.excel.ProductExcel;
import org.springblade.thingsphere.manage.pojo.dto.MetaDataDTO;
import org.springblade.thingsphere.manage.pojo.entity.ProductEntity;
import org.springblade.thingsphere.manage.pojo.req.AddObjectModelRequestBody;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailInfo;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailVO;
import org.springblade.thingsphere.manage.pojo.vo.ProductVO;

import java.util.List;

/**
 * 产品 服务类
 *
 * @author lhb
 * @since 2024-10-08
 */
public interface IProductService extends BaseService<ProductEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param product
	 * @return
	 */
	IPage<ProductVO> selectProductPage(IPage<ProductVO> page, ProductVO product);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ProductExcel> exportProduct(Wrapper<ProductEntity> queryWrapper);

	ProductDetailVO productDetail(Long id);

	ProductDetailInfo productDetailInfo(Long id);

	Boolean updateStatus(Long id, Integer status);

	/**
	 * 保存物模型-属性
	 *
	 * @param body
	 * @return
	 */
	boolean addObjectModel(AddObjectModelRequestBody body);

	/**
	 * 物模型-属性列表
	 *
	 * @param productId
	 * @return
	 */
	List<MetaDataDTO.PropertiesDTO> propertiesAttributes(Long productId);

	/**
	 * 属性物模型详情
	 *
	 * @param productId
	 * @param propertiesId
	 * @return
	 */
	MetaDataDTO.PropertiesDTO propertiesAttributesDetail(Long productId, String propertiesId);

	/**
	 * 删除属性物模
	 *
	 * @param productId
	 * @param propertiesId
	 * @return
	 */
	boolean delObjectModel(Long productId, String propertiesId);
}
