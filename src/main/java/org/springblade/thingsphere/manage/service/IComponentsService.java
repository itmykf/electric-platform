package org.springblade.thingsphere.manage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.core.tool.api.R;
import org.springblade.thingsphere.manage.excel.ComponentsExcel;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;

import java.util.List;

/**
 * 网络组件 服务类
 *
 * @author lhb
 * @since 2024-09-27
 */
public interface IComponentsService extends BaseService<ComponentsEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param components
	 * @return
	 */
	IPage<ComponentsVO> selectComponentsPage(IPage<ComponentsVO> page, ComponentsVO components);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ComponentsExcel> exportComponents(Wrapper<ComponentsEntity> queryWrapper);

	/**
	 * 启用
	 *
	 * @param componentsVO
	 * @return
	 */
	R<Boolean> start(ComponentsVO componentsVO);

	/**
	 * 停用
	 *
	 * @param componentsVO
	 * @return
	 */
	R<Boolean> stop(ComponentsVO componentsVO);

	List<ComponentsEntity> dictList();

	boolean dictClear();
}
