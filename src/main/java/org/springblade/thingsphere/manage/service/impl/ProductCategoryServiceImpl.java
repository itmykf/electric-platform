/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.service.impl;

import org.springblade.thingsphere.manage.pojo.entity.ProductCategoryEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProductCategoryVO;
import org.springblade.thingsphere.manage.excel.ProductCategoryExcel;
import org.springblade.thingsphere.manage.mapper.ProductCategoryMapper;
import org.springblade.thingsphere.manage.service.IProductCategoryService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseServiceImpl;
import java.util.List;

/**
 * 产品分类 服务实现类
 *
 * @author lhb
 * @since 2024-10-08
 */
@Service
public class ProductCategoryServiceImpl extends BaseServiceImpl<ProductCategoryMapper, ProductCategoryEntity> implements IProductCategoryService {

	@Override
	public IPage<ProductCategoryVO> selectProductCategoryPage(IPage<ProductCategoryVO> page, ProductCategoryVO productCategory) {
		return page.setRecords(baseMapper.selectProductCategoryPage(page, productCategory));
	}


	@Override
	public List<ProductCategoryExcel> exportProductCategory(Wrapper<ProductCategoryEntity> queryWrapper) {
		List<ProductCategoryExcel> productCategoryList = baseMapper.exportProductCategory(queryWrapper);
		//productCategoryList.forEach(productCategory -> {
		//	productCategory.setTypeName(DictCache.getValue(DictEnum.YES_NO, ProductCategory.getType()));
		//});
		return productCategoryList;
	}

}
