package org.springblade.thingsphere.manage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.thingsphere.manage.excel.GatewayExcel;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;
import org.springblade.thingsphere.manage.pojo.vo.GatewayVO;

import java.util.List;

/**
 * 设备网关 服务类
 *
 * @author lhb
 * @since 2024-09-30
 */
public interface IGatewayService extends BaseService<GatewayEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param gateway
	 * @return
	 */
	IPage<GatewayVO> selectGatewayPage(IPage<GatewayVO> page, GatewayVO gateway);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<GatewayExcel> exportGateway(Wrapper<GatewayEntity> queryWrapper);

	boolean start(ComponentsVO componentsVO);

	boolean stop(ComponentsVO componentsVO);


	List<GatewayEntity> dictList();

	GatewayEntity dictByNetworkId(Long networkId);

	boolean dictClear();
}
