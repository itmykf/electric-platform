package org.springblade.thingsphere.manage.service.impl;

import cn.hutool.cache.Cache;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.collections.CollectionUtils;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.thingsphere.manage.excel.GatewayExcel;
import org.springblade.thingsphere.manage.mapper.GatewayMapper;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;
import org.springblade.thingsphere.manage.pojo.vo.GatewayVO;
import org.springblade.thingsphere.manage.service.IGatewayService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 设备网关 服务实现类
 *
 * @author lhb
 * @since 2024-09-30
 */
@Service
public class GatewayServiceImpl extends BaseServiceImpl<GatewayMapper, GatewayEntity> implements IGatewayService {

	private static final String CACHE_NAME = "iot:gateway";

	@Override
	public IPage<GatewayVO> selectGatewayPage(IPage<GatewayVO> page, GatewayVO gateway) {
		return page.setRecords(baseMapper.selectGatewayPage(page, gateway));
	}


	@Override
	public List<GatewayExcel> exportGateway(Wrapper<GatewayEntity> queryWrapper) {
		List<GatewayExcel> gatewayList = baseMapper.exportGateway(queryWrapper);
		//gatewayList.forEach(gateway -> {
		//	gateway.setTypeName(DictCache.getValue(DictEnum.YES_NO, Gateway.getType()));
		//});
		return gatewayList;
	}

	@Override
	public boolean start(ComponentsVO componentsVO) {
		GatewayEntity gateway = this.getById(componentsVO.getId());
		if (1 == gateway.getStatus()) {
			throw new ServiceException("网关已启用");
		}
		gateway.setStatus(1);
		this.dictClear();
		return this.updateById(gateway);
	}

	@Override
	public boolean stop(ComponentsVO componentsVO) {
		GatewayEntity gateway = this.getById(componentsVO.getId());
		if (0 == gateway.getStatus()) {
			throw new ServiceException("网关已停用");
		}
		gateway.setStatus(0);
		this.dictClear();
		return this.updateById(gateway);
	}

	private static final Cache<String, List<GatewayEntity>> fifoCache = cn.hutool.cache.CacheUtil.newFIFOCache(1000);

	@Override
	public boolean dictClear() {
		fifoCache.clear();
		return true;
	}

	@Override
	public List<GatewayEntity> dictList() {
		if (fifoCache.containsKey(CACHE_NAME)) {
			return fifoCache.get(CACHE_NAME);
		}

		List<GatewayEntity> list = this.list(Wrappers.<GatewayEntity>lambdaQuery().eq(GatewayEntity::getStatus, 1));
		fifoCache.put(CACHE_NAME, list);
		return list;
	}

	@Override
	public GatewayEntity dictByNetworkId(Long networkId) {
		String key = StrUtil.toString(networkId);
		if (fifoCache.containsKey(key)) {
			List<GatewayEntity> gatewayEntities = fifoCache.get(key);
			if (CollectionUtils.isEmpty(gatewayEntities)) {
				return null;
			}
			return gatewayEntities.get(0);
		}

		GatewayEntity gateway = this.getOne(
			Wrappers.<GatewayEntity>lambdaQuery()
				.eq(GatewayEntity::getNetworkId, networkId)
				.eq(GatewayEntity::getStatus, 1)
		);
		if (null == gateway) {
			fifoCache.put(key, new ArrayList<>());
			return null;
		}
		fifoCache.put(key, List.of(gateway));
		return gateway;
	}

}
