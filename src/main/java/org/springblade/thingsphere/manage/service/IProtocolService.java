package org.springblade.thingsphere.manage.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.core.oss.model.BladeFile;
import org.springblade.core.tool.api.R;
import org.springblade.thingsphere.manage.excel.ProtocolExcel;
import org.springblade.thingsphere.manage.pojo.entity.ProtocolEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProtocolVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 协议管理 服务类
 *
 * @author lhb
 * @since 2024-09-26
 */
public interface IProtocolService extends BaseService<ProtocolEntity> {
	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param protocol
	 * @return
	 */
	IPage<ProtocolVO> selectProtocolPage(IPage<ProtocolVO> page, ProtocolVO protocol);


	/**
	 * 导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ProtocolExcel> exportProtocol(Wrapper<ProtocolEntity> queryWrapper);

	/**
	 * 协议包保存
	 *
	 * @param file
	 * @return
	 */
	BladeFile putFile(MultipartFile file);

	/**
	 * 发布协议
	 *
	 * @param protocol
	 * @return
	 */
	R<Boolean> publish(ProtocolEntity protocol);

	/**
	 * 取消发布协议
	 *
	 * @param protocol
	 * @return
	 */
	R<Boolean> unpublish(ProtocolEntity protocol);

	List<ProtocolEntity> dictList();

	boolean dictClear();
}
