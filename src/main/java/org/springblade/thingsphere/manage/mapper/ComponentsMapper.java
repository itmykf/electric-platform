package org.springblade.thingsphere.manage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.thingsphere.manage.excel.ComponentsExcel;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;

import java.util.List;

/**
 * 网络组件 Mapper 接口
 *
 * @author lhb
 * @since 2024-09-27
 */
public interface ComponentsMapper extends BaseMapper<ComponentsEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param components
	 * @return
	 */
	List<ComponentsVO> selectComponentsPage(IPage page, ComponentsVO components);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ComponentsExcel> exportComponents(@Param("ew") Wrapper<ComponentsEntity> queryWrapper);

}
