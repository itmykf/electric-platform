package org.springblade.thingsphere.manage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.thingsphere.manage.excel.ProtocolExcel;
import org.springblade.thingsphere.manage.pojo.entity.ProtocolEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProtocolVO;

import java.util.List;

/**
 * 协议管理 Mapper 接口
 *
 * @author lhb
 * @since 2024-09-26
 */
public interface ProtocolMapper extends BaseMapper<ProtocolEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param protocol
	 * @return
	 */
	List<ProtocolVO> selectProtocolPage(IPage page, ProtocolVO protocol);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ProtocolExcel> exportProtocol(@Param("ew") Wrapper<ProtocolEntity> queryWrapper);

}
