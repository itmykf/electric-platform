package org.springblade.thingsphere.manage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.thingsphere.manage.excel.GatewayExcel;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;
import org.springblade.thingsphere.manage.pojo.vo.GatewayVO;

import java.util.List;

/**
 * 设备网关 Mapper 接口
 *
 * @author lhb
 * @since 2024-09-30
 */
public interface GatewayMapper extends BaseMapper<GatewayEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param gateway
	 * @return
	 */
	List<GatewayVO> selectGatewayPage(IPage page, GatewayVO gateway);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<GatewayExcel> exportGateway(@Param("ew") Wrapper<GatewayEntity> queryWrapper);

}
