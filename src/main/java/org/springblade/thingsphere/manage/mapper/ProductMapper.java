/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import org.springblade.thingsphere.manage.excel.ProductExcel;
import org.springblade.thingsphere.manage.pojo.entity.ProductEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailInfo;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailVO;
import org.springblade.thingsphere.manage.pojo.vo.ProductVO;

import java.util.List;

/**
 * 产品 Mapper 接口
 *
 * @author lhb
 * @since 2024-10-08
 */
public interface ProductMapper extends BaseMapper<ProductEntity> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param product
	 * @return
	 */
	List<ProductVO> selectProductPage(IPage page, ProductVO product);


	/**
	 * 获取导出数据
	 *
	 * @param queryWrapper
	 * @return
	 */
	List<ProductExcel> exportProduct(@Param("ew") Wrapper<ProductEntity> queryWrapper);

	ProductDetailVO productDetail(@Param("id") Long id);

	ProductDetailInfo productDetailInfo(@Param("id") Long id);
}
