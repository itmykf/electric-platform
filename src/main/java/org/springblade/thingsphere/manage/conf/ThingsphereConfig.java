package org.springblade.thingsphere.manage.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lhb
 * @date 2024/9/26 下午6:07
 */
@Data
@Component
@ConfigurationProperties(prefix = "thingsphere")
public class ThingsphereConfig {
	// 协议包相关配置
	private Protocol protocol;

	@Data
	public static class Protocol {
		// 协议包上传路径
		private String localPath;
	}

}
