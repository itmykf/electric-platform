package org.springblade.thingsphere.manage.pojo.req;

import lombok.Data;
import org.springblade.thingsphere.manage.pojo.dto.MetaDataDTO;

import java.util.Map;

/**
 * @author lhb
 * @date 2024/10/10 下午2:31
 */
@Data
public class AddObjectModelRequestBody {
	// 产品ID
	private Long productId;

	// 保存类型 1 仅保存 2保存并应用
	private Integer saveType;

	// 属性物模型
	private MetaDataDTO.PropertiesDTO properties;

	// 功能物模型
	private Map<String, Object> function;

}
