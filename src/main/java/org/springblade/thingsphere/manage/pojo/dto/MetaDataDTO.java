package org.springblade.thingsphere.manage.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lhb
 * @date 2024/10/10 下午2:54
 */
@NoArgsConstructor
@Data
public class MetaDataDTO {

	@JsonProperty("properties")
	private List<PropertiesDTO> properties = new ArrayList<>();
//	@JsonProperty("functions")
//	private List<?> functions;

	@NoArgsConstructor
	@Data
	public static class PropertiesDTO {
		@JsonProperty("id")
		private String id;
		@JsonProperty("name")
		private String name;
		@JsonProperty("valueType")
		private ValueTypeDTO valueType;
		@JsonProperty("expands")
		private ExpandsDTO expands;

		private String description;

		@NoArgsConstructor
		@Data
		public static class ValueTypeDTO {
			@JsonProperty("type")
			private String type;
			@JsonProperty("scale")
			private Integer scale;
			@JsonProperty("unit")
			private String unit;
			// string类型
			private Expands expands;
			// 枚举 enum
			private List<Elements> elements;
			// boolean类型
			private String trueText;
			private String trueValue;
			private String falseText;
			private String falseValue;

			// date类型
			private String format;

			@NoArgsConstructor
			@Data
			public static class Elements {
				private String text;
				private String value;
				private Long id;
			}

			@NoArgsConstructor
			@Data
			public static class Expands {
				private String maxLength;
			}
		}


		@NoArgsConstructor
		@Data
		public static class ExpandsDTO {
			@JsonProperty("readOnly")
			private String readOnly;
		}
	}
}
