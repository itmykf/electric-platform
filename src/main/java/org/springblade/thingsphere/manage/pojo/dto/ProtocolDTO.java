package org.springblade.thingsphere.manage.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.ProtocolEntity;

import java.io.Serial;

/**
 * 协议管理 数据传输对象实体类
 *
 * @author lhb
 * @since 2024-09-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProtocolDTO extends ProtocolEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
