package org.springblade.thingsphere.manage.pojo.dto;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/27 下午5:20
 */
@Data
public class ComponentConfig {

	private Integer threadNum;
	private String clientId;
	private String host;
	private String port;

	private String reportPropertyPath;

	private String userName;
	private String password;
}
