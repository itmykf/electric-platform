package org.springblade.thingsphere.manage.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;

import java.io.Serial;

/**
 * 网络组件 数据传输对象实体类
 *
 * @author lhb
 * @since 2024-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ComponentsDTO extends ComponentsEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
