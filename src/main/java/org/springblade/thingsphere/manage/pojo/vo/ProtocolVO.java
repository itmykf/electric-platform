package org.springblade.thingsphere.manage.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.ProtocolEntity;

import java.io.Serial;

/**
 * 协议管理 视图实体类
 *
 * @author lhb
 * @since 2024-09-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProtocolVO extends ProtocolEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
