package org.springblade.thingsphere.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 协议管理 实体类
 *
 * @author lhb
 * @since 2024-09-26
 */
@Data
@TableName("iot_protocol")
@Schema(description = "Protocol对象")
@EqualsAndHashCode(callSuper = true)
public class ProtocolEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 协议包唯一标识
	 */
	@Schema(description = "协议包唯一标识")
	private String protocolId;
	/**
	 *
	 */
	@Schema(description = "")
	private String name;
	/**
	 * jar地址
	 */
	@Schema(description = "jar地址")
	private String jarPath;
	/**
	 * 类路径
	 */
	@Schema(description = "类路径")
	private String clazzName;
	/**
	 *
	 */
	@Schema(description = "")
	private String description;

}
