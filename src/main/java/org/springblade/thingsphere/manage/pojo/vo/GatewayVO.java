package org.springblade.thingsphere.manage.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;

import java.io.Serial;

/**
 * 设备网关 视图实体类
 *
 * @author lhb
 * @since 2024-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GatewayVO extends GatewayEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
