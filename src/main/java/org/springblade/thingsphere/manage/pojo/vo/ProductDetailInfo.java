package org.springblade.thingsphere.manage.pojo.vo;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/9 下午4:25
 */
@Data
public class ProductDetailInfo {

	private Long id;
	private String productId;
	private String productName;

	private String classifiedName;
	private String protocolName;

	private String transportProtocol;
	private String storePolicyName;

	private String description;

}
