/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 产品 实体类
 *
 * @author lhb
 * @since 2024-10-08
 */
@Data
@TableName("iot_product")
@Schema(description = "Product对象")
@EqualsAndHashCode(callSuper = true)
public class ProductEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 分类ID
	 */
	@Schema(description = "分类ID")
	private Long classifiedId;
	/**
	 * 产品ID
	 */
	@Schema(description = "产品ID")
	private String productId;
	/**
	 * 名称
	 */
	@Schema(description = "名称")
	private String productName;
	/**
	 * 数据存储策略 不存储: none  行式存储: row
	 */
	@Schema(description = "数据存储策略 不存储: none  行式存储: row")
	private String storePolicy;
	/**
	 * 物模型
	 */
	@Schema(description = "物模型")
	private String metadata;

	/**
	 * 物模型-应用
	 */
	@Schema(description = "物模型-应用")
	private String metadataUse;

	/**
	 * 协议配置
	 */
	@Schema(description = "协议配置")
	private String configuration;
	/**
	 * 入网方式: 直连,组网...
	 */
	@Schema(description = "入网方式: 直连,组网...")
	private String networkWay;
	/**
	 * 设备类型: 网关，设备
	 */
	@Schema(description = "设备类型: 网关，设备")
	private String deviceType;
	/**
	 * 数据存储策略配置
	 */
	@Schema(description = "数据存储策略配置")
	private String storePolicyConfiguration;
	/**
	 * 消息协议
	 */
	@Schema(description = "消息协议")
	private Long messageProtocol;
	/**
	 * 传输协议: MQTT,COAP,UDP
	 */
	@Schema(description = "传输协议: MQTT,COAP,UDP")
	private String transportProtocol;
	/**
	 * 图片地址
	 */
	@Schema(description = "图片地址")
	private String photoUrl;
	/**
	 * 说明
	 */
	@Schema(description = "说明")
	private String description;

}
