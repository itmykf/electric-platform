package org.springblade.thingsphere.manage.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;

import java.io.Serial;

/**
 * 网络组件 视图实体类
 *
 * @author lhb
 * @since 2024-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ComponentsVO extends ComponentsEntity {
	@Serial
	private static final long serialVersionUID = 1L;

	private Integer threadNum;
	private String clientId;
	private String host;
	private String port;

	private String reportPropertyPath;

	private String userName;
	private String password;
}
