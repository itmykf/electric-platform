package org.springblade.thingsphere.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 设备网关 实体类
 *
 * @author lhb
 * @since 2024-09-30
 */
@Data
@TableName("iot_gateway")
@Schema(description = "Gateway对象")
@EqualsAndHashCode(callSuper = true)
public class GatewayEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	@Schema(description = "名称")
	private String name;

	/**
	 * 类型
	 */
	@Schema(description = "类型")
	private String type;
	/**
	 * 网络组件ID
	 */
	@Schema(description = "网络组件ID")
	private Long networkId;
	/**
	 * 协议ID
	 */
	@Schema(description = "协议ID")
	private Long protocolId;
	/**
	 * 说明
	 */
	@Schema(description = "说明")
	private String description;

}
