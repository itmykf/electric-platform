package org.springblade.thingsphere.manage.pojo.vo;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/9 上午11:29
 */
@Data
public class ProductDetailVO {
	private Long id;
	private String productId;
	private String productName;
	private String deviceCount;
	private Integer status;
}
