package org.springblade.thingsphere.manage.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.core.tenant.mp.TenantEntity;

import java.io.Serial;

/**
 * 网络组件 实体类
 *
 * @author lhb
 * @since 2024-09-27
 */
@Data
@TableName("iot_components")
@Schema(description = "Components对象")
@EqualsAndHashCode(callSuper = true)
public class ComponentsEntity extends TenantEntity {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 组件名称
	 */
	@Schema(description = "组件名称")
	private String name;
	/**
	 * 组件类型
	 */
	@Schema(description = "组件类型")
	private String type;
	/**
	 *
	 */
	@Schema(description = "")
	private String configuration;
	/**
	 * 备注
	 */
	@Schema(description = "备注")
	private String description;

}
