package org.springblade.thingsphere.manage.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;

import java.io.Serial;

/**
 * 设备网关 数据传输对象实体类
 *
 * @author lhb
 * @since 2024-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GatewayDTO extends GatewayEntity {
	@Serial
	private static final long serialVersionUID = 1L;

}
