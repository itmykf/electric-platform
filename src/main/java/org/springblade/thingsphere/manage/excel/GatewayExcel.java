package org.springblade.thingsphere.manage.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 设备网关 Excel实体类
 *
 * @author lhb
 * @since 2024-09-30
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class GatewayExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@ColumnWidth(20)
	@ExcelProperty("名称")
	private String name;

	/**
	 * 类型
	 */
	@ColumnWidth(20)
	@ExcelProperty("类型")
	private String type;
	/**
	 * 网络组件ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("网络组件ID")
	private Long networkId;
	/**
	 * 协议ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("协议ID")
	private Long protocolId;
	/**
	 * 说明
	 */
	@ColumnWidth(20)
	@ExcelProperty("说明")
	private String description;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;
	/**
	 * 是否已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除")
	private Integer isDeleted;

}
