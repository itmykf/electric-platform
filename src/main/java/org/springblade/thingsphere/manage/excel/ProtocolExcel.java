package org.springblade.thingsphere.manage.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 协议管理 Excel实体类
 *
 * @author lhb
 * @since 2024-09-26
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ProtocolExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 协议包唯一标识
	 */
	@ColumnWidth(20)
	@ExcelProperty("协议包唯一标识")
	private String protocolId;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String name;
	/**
	 * jar地址
	 */
	@ColumnWidth(20)
	@ExcelProperty("jar地址")
	private String configuration;
	/**
	 * 类路径
	 */
	@ColumnWidth(20)
	@ExcelProperty("类路径")
	private String clazzName;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String description;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;
	/**
	 * 是否已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除")
	private Integer isDeleted;

}
