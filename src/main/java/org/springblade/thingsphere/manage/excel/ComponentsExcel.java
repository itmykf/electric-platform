package org.springblade.thingsphere.manage.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 网络组件 Excel实体类
 *
 * @author lhb
 * @since 2024-09-27
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ComponentsExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 组件名称
	 */
	@ColumnWidth(20)
	@ExcelProperty("组件名称")
	private String name;
	/**
	 * 组件类型
	 */
	@ColumnWidth(20)
	@ExcelProperty("组件类型")
	private String type;
	/**
	 *
	 */
	@ColumnWidth(20)
	@ExcelProperty("")
	private String configuration;
	/**
	 * 备注
	 */
	@ColumnWidth(20)
	@ExcelProperty("备注")
	private String description;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;
	/**
	 * 是否已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除")
	private Integer isDeleted;

}
