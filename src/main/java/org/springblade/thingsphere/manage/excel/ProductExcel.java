/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;


/**
 * 产品 Excel实体类
 *
 * @author lhb
 * @since 2024-10-08
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class ProductExcel implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 分类ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("分类ID")
	private Long classifiedId;
	/**
	 * 产品ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("产品ID")
	private String productId;
	/**
	 * 名称
	 */
	@ColumnWidth(20)
	@ExcelProperty("名称")
	private String productName;
	/**
	 * 数据存储策略 不存储: none  行式存储: row
	 */
	@ColumnWidth(20)
	@ExcelProperty("数据存储策略 不存储: none  行式存储: row")
	private String storePolicy;
	/**
	 * 物模型
	 */
	@ColumnWidth(20)
	@ExcelProperty("物模型")
	private String metadata;
	/**
	 * 协议配置
	 */
	@ColumnWidth(20)
	@ExcelProperty("协议配置")
	private String configuration;
	/**
	 * 入网方式: 直连,组网...
	 */
	@ColumnWidth(20)
	@ExcelProperty("入网方式: 直连,组网...")
	private String networkWay;
	/**
	 * 设备类型: 网关，设备
	 */
	@ColumnWidth(20)
	@ExcelProperty("设备类型: 网关，设备")
	private String deviceType;
	/**
	 * 数据存储策略配置
	 */
	@ColumnWidth(20)
	@ExcelProperty("数据存储策略配置")
	private String storePolicyConfiguration;
	/**
	 * 消息协议
	 */
	@ColumnWidth(20)
	@ExcelProperty("消息协议")
	private String messageProtocol;
	/**
	 * 传输协议: MQTT,COAP,UDP
	 */
	@ColumnWidth(20)
	@ExcelProperty("传输协议: MQTT,COAP,UDP")
	private String transportProtocol;
	/**
	 * 图片地址
	 */
	@ColumnWidth(20)
	@ExcelProperty("图片地址")
	private String photoUrl;
	/**
	 * 说明
	 */
	@ColumnWidth(20)
	@ExcelProperty("说明")
	private String describe;
	/**
	 * 租户ID
	 */
	@ColumnWidth(20)
	@ExcelProperty("租户ID")
	private String tenantId;
	/**
	 * 是否已删除
	 */
	@ColumnWidth(20)
	@ExcelProperty("是否已删除")
	private Integer isDeleted;

}
