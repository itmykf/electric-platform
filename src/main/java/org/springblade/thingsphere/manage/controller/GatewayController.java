package org.springblade.thingsphere.manage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.cache.utils.CacheUtil;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.thingsphere.manage.excel.GatewayExcel;
import org.springblade.thingsphere.manage.pojo.entity.GatewayEntity;
import org.springblade.thingsphere.manage.pojo.vo.ComponentsVO;
import org.springblade.thingsphere.manage.pojo.vo.GatewayVO;
import org.springblade.thingsphere.manage.service.IGatewayService;
import org.springblade.thingsphere.manage.wrapper.GatewayWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 设备网关 控制器
 *
 * @author lhb
 * @since 2024-09-30
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-gateway/gateway")
@Tag(name = "设备网关", description = "设备网关接口")
public class GatewayController extends BladeController {

	private final IGatewayService gatewayService;

	/**
	 * 设备网关 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入gateway")
	public R<GatewayVO> detail(GatewayEntity gateway) {
		GatewayEntity detail = gatewayService.getOne(Condition.getQueryWrapper(gateway));
		return R.data(GatewayWrapper.build().entityVO(detail));
	}

	/**
	 * 设备网关 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入gateway")
	public R<IPage<GatewayVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> gateway, Query query) {
		IPage<GatewayEntity> pages = gatewayService.page(Condition.getPage(query), Condition.getQueryWrapper(gateway, GatewayEntity.class));
		return R.data(GatewayWrapper.build().pageVO(pages));
	}

	/**
	 * 设备网关 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入gateway")
	public R submit(@Valid @RequestBody GatewayEntity gateway) {
		gatewayService.dictClear();
		return R.status(gatewayService.saveOrUpdate(gateway));
	}

	/**
	 * 设备网关 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		gatewayService.dictClear();
		return R.status(gatewayService.deleteLogic(Func.toLongList(ids)));
	}

	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-gateway")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入gateway")
	public void exportGateway(@Parameter(hidden = true) @RequestParam Map<String, Object> gateway, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<GatewayEntity> queryWrapper = Condition.getQueryWrapper(gateway, GatewayEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(Gateway::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(GatewayEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<GatewayExcel> list = gatewayService.exportGateway(queryWrapper);
		ExcelUtil.export(response, "设备网关数据" + DateUtil.time(), "设备网关数据表", list, GatewayExcel.class);
	}

	/**
	 * 启用
	 *
	 * @param componentsVO
	 * @return
	 */
	@PostMapping("/start")
	public R<Boolean> start(@Valid @RequestBody ComponentsVO componentsVO) {
		CacheUtil.get("", "", "");
		return R.status(gatewayService.start(componentsVO));
	}

	/**
	 * 停用
	 */
	@PostMapping("/stop")
	public R<Boolean> stop(@Valid @RequestBody ComponentsVO componentsVO) {
		return R.status(gatewayService.stop(componentsVO));
	}
}
