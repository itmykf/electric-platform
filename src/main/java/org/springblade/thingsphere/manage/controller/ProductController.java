package org.springblade.thingsphere.manage.controller;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.thingsphere.manage.excel.ProductExcel;
import org.springblade.thingsphere.manage.pojo.dto.MetaDataDTO;
import org.springblade.thingsphere.manage.pojo.entity.ProductEntity;
import org.springblade.thingsphere.manage.pojo.req.AddObjectModelRequestBody;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailInfo;
import org.springblade.thingsphere.manage.pojo.vo.ProductDetailVO;
import org.springblade.thingsphere.manage.pojo.vo.ProductVO;
import org.springblade.thingsphere.manage.service.IProductService;
import org.springblade.thingsphere.manage.wrapper.ProductWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 产品 控制器
 *
 * @author lhb
 * @since 2024-10-08
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-product/product")
@Tag(name = "产品", description = "产品接口")
public class ProductController extends BladeController {

	private final IProductService productService;

	/**
	 * 产品 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入product")
	public R<ProductVO> detail(ProductEntity product) {
		ProductEntity detail = productService.getOne(Condition.getQueryWrapper(product));
		return R.data(ProductWrapper.build().entityVO(detail));
	}

	/**
	 * 产品 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入product")
	public R<IPage<ProductVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> product, Query query) {
		IPage<ProductEntity> pages = productService.page(Condition.getPage(query), Condition.getQueryWrapper(product, ProductEntity.class));
		return R.data(ProductWrapper.build().pageVO(pages));
	}

	/**
	 * 产品 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入product")
	public R submit(@Valid @RequestBody ProductEntity product) {

		if (null == product.getId()) {
			MetaDataDTO metaDataDTO = new MetaDataDTO();
			product.setMetadata(JSONObject.toJSONString(metaDataDTO));
		}
		return R.status(productService.saveOrUpdate(product));
	}

	/**
	 * 产品 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(productService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-product")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入product")
	public void exportProduct(@Parameter(hidden = true) @RequestParam Map<String, Object> product, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<ProductEntity> queryWrapper = Condition.getQueryWrapper(product, ProductEntity.class);
		queryWrapper.lambda().eq(ProductEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<ProductExcel> list = productService.exportProduct(queryWrapper);
		ExcelUtil.export(response, "产品数据" + DateUtil.time(), "产品数据表", list, ProductExcel.class);
	}

	@GetMapping("/productDetail/{id}")
	@ApiOperationSupport(order = 10)
	@Operation(summary = "产品详情")
	public R<ProductDetailVO> productDetail(@PathVariable("id") Long id) {
		return R.data(productService.productDetail(id));
	}

	@GetMapping("/productDetailInfo/{id}")
	@ApiOperationSupport(order = 10)
	@Operation(summary = "产品详情(二级页面)")
	public R<ProductDetailInfo> productDetailInfo(@PathVariable("id") Long id) {
		return R.data(productService.productDetailInfo(id));
	}

	@GetMapping("/updateStatus/{id}/{status}")
	@ApiOperationSupport(order = 10)
	@Operation(summary = "启用停用")
	public R<Boolean> updateStatus(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
		return R.data(productService.updateStatus(id, status));
	}

	@GetMapping("/propertiesAttributes/{productId}")
	@ApiOperationSupport(order = 20)
	@Operation(summary = "物模型属性列表", description = "物模型属性列表")
	public R<List<MetaDataDTO.PropertiesDTO>> propertiesAttributes(@PathVariable("productId") Long productId) {
		return R.data(productService.propertiesAttributes(productId));
	}

	@GetMapping("/propertiesAttributesDetail/{productId}/{propertiesId}")
	@ApiOperationSupport(order = 21)
	@Operation(summary = "属性物模型详情", description = "属性物模型详情")
	public R<MetaDataDTO.PropertiesDTO> propertiesAttributesDetail(@PathVariable("productId") Long productId, @PathVariable("propertiesId") String propertiesId) {
		return R.data(productService.propertiesAttributesDetail(productId, propertiesId));
	}

	@PostMapping("/addObjectModel")
	@ApiOperationSupport(order = 22)
	@Operation(summary = "保存物模型", description = "保存物模型")
	public R addObjectModel(@Valid @RequestBody AddObjectModelRequestBody body) {
		return R.status(productService.addObjectModel(body));
	}

	@PostMapping("/delObjectModel/{productId}/{propertiesId}")
	@ApiOperationSupport(order = 23)
	@Operation(summary = "删除物模型", description = "删除物模型")
	public R delObjectModel(@PathVariable("productId") Long productId, @PathVariable("propertiesId") String propertiesId) {
		return R.status(productService.delObjectModel(productId, propertiesId));
	}

}
