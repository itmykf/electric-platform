/**
 * BladeX Commercial License Agreement
 * Copyright (c) 2018-2099, https://bladex.cn. All rights reserved.
 * <p>
 * Use of this software is governed by the Commercial License Agreement
 * obtained after purchasing a license from BladeX.
 * <p>
 * 1. This software is for development use only under a valid license
 * from BladeX.
 * <p>
 * 2. Redistribution of this software's source code to any third party
 * without a commercial license is strictly prohibited.
 * <p>
 * 3. Licensees may copyright their own code but cannot use segments
 * from this software for such purposes. Copyright of this software
 * remains with BladeX.
 * <p>
 * Using this software signifies agreement to this License, and the software
 * must not be used for illegal purposes.
 * <p>
 * THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY. The author is
 * not liable for any claims arising from secondary or illegal development.
 * <p>
 * Author: Chill Zhuang (bladejava@qq.com)
 */
package org.springblade.thingsphere.manage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.excel.util.ExcelUtil;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.thingsphere.manage.excel.ProductCategoryExcel;
import org.springblade.thingsphere.manage.pojo.entity.ProductCategoryEntity;
import org.springblade.thingsphere.manage.pojo.vo.ProductCategoryVO;
import org.springblade.thingsphere.manage.service.IProductCategoryService;
import org.springblade.thingsphere.manage.wrapper.ProductCategoryWrapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 产品分类 控制器
 *
 * @author lhb
 * @since 2024-10-08
 */
@RestController
@AllArgsConstructor
@RequestMapping("blade-productcategory/productCategory")
@Tag(name = "产品分类", description = "产品分类接口")
public class ProductCategoryController extends BladeController {

	private final IProductCategoryService productCategoryService;

	/**
	 * 产品分类 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@Operation(summary = "详情", description = "传入productCategory")
	public R<ProductCategoryVO> detail(ProductCategoryEntity productCategory) {
		ProductCategoryEntity detail = productCategoryService.getOne(Condition.getQueryWrapper(productCategory));
		return R.data(ProductCategoryWrapper.build().entityVO(detail));
	}

	/**
	 * 产品分类 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@Operation(summary = "分页", description = "传入productCategory")
	public R<IPage<ProductCategoryVO>> list(@Parameter(hidden = true) @RequestParam Map<String, Object> productCategory, Query query) {
		IPage<ProductCategoryEntity> pages = productCategoryService.page(Condition.getPage(query), Condition.getQueryWrapper(productCategory, ProductCategoryEntity.class));
		return R.data(ProductCategoryWrapper.build().pageVO(pages));
	}

	/**
	 * 产品分类 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@Operation(summary = "新增或修改", description = "传入productCategory")
	public R submit(@Valid @RequestBody ProductCategoryEntity productCategory) {
		return R.status(productCategoryService.saveOrUpdate(productCategory));
	}

	/**
	 * 产品分类 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "逻辑删除", description = "传入ids")
	public R remove(@Parameter(description = "主键集合", required = true) @RequestParam String ids) {
		return R.status(productCategoryService.deleteLogic(Func.toLongList(ids)));
	}


	/**
	 * 导出数据
	 */
	@PreAuth(RoleConstant.HAS_ROLE_ADMIN)
	@GetMapping("/export-productCategory")
	@ApiOperationSupport(order = 9)
	@Operation(summary = "导出数据", description = "传入productCategory")
	public void exportProductCategory(@Parameter(hidden = true) @RequestParam Map<String, Object> productCategory, BladeUser bladeUser, HttpServletResponse response) {
		QueryWrapper<ProductCategoryEntity> queryWrapper = Condition.getQueryWrapper(productCategory, ProductCategoryEntity.class);
		//if (!AuthUtil.isAdministrator()) {
		//	queryWrapper.lambda().eq(ProductCategory::getTenantId, bladeUser.getTenantId());
		//}
		queryWrapper.lambda().eq(ProductCategoryEntity::getIsDeleted, BladeConstant.DB_NOT_DELETED);
		List<ProductCategoryExcel> list = productCategoryService.exportProductCategory(queryWrapper);
		ExcelUtil.export(response, "产品分类数据" + DateUtil.time(), "产品分类数据表", list, ProductCategoryExcel.class);
	}

	@GetMapping("/dictList")
	@ApiOperationSupport(order = 7)
	@Operation(summary = "字典列表", description = "传入ids")
	public R dictList() {
		return R.data(productCategoryService.list());
	}

}
