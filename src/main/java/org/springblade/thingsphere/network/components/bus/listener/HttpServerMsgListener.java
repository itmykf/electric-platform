package org.springblade.thingsphere.network.components.bus.listener;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springblade.thingsphere.domain.ReportPropertyMessage;
import org.springblade.thingsphere.network.components.bus.event.HttpServerMsgEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lhb
 * @date 2024/9/29 下午3:06
 */
@Slf4j
@Component
@AllArgsConstructor
public class HttpServerMsgListener implements ApplicationListener<HttpServerMsgEvent<List<ReportPropertyMessage>>> {

	private final HandlerListenerImpl handlerListener;

	@Override
	public void onApplicationEvent(HttpServerMsgEvent<List<ReportPropertyMessage>> event) {
		log.info("http server msg - {}", event.getMsg());
		handlerListener.handlerMessages(event.getMsg());
	}
}
