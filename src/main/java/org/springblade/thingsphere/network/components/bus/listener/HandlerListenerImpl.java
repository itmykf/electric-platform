package org.springblade.thingsphere.network.components.bus.listener;

import com.alibaba.fastjson2.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springblade.thingsphere.domain.ReportPropertyMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lhb
 * @date 2024/10/10 下午6:31
 */
@Component
public class HandlerListenerImpl {

	private static final Logger log = LoggerFactory.getLogger(HandlerListenerImpl.class);

	@Async
	public void handlerMessages(List<ReportPropertyMessage> handlerMessages) {
		log.info("handlerMessages:{}", JSONObject.toJSONString(handlerMessages));
		// 查询设备对应的产品

		// 保存到ES

		// 是否同步更新实时数据到数据库

	}
}
