package org.springblade.thingsphere.network.components.common;

import io.vertx.core.http.HttpServerOptions;
import lombok.*;

import java.util.Collections;
import java.util.Map;

/**
 * @author lhb
 * @date 2024/9/29 上午10:01
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpServerConfig {

	/**
	 * 服务配置ID
	 */
	private Long id;

	/**
	 * 服务实例数量(线程数)
	 */
	private int instance = Math.max(4, Runtime.getRuntime().availableProcessors());

	/**
	 * 绑定端口
	 */
	private int port;

	/**
	 * 绑定网卡
	 */
	private String host = "0.0.0.0";

	/**
	 * 是否开启SSL
	 */
	private boolean ssl;

	/**
	 * 证书ID
	 *
	 * @see Certificate#getId()
	 * @see org.jetlinks.pro.network.security.CertificateManager
	 */
	private String certId;


	/**
	 * HTTP服务其他配置
	 */
	private HttpServerOptions options;

	/**
	 * 固定响应头信息
	 */
	private Map<String, String> httpHeaders;

	public Map<String, String> getHttpHeaders() {
		return nullMapHandle(httpHeaders);
	}

	private Map<String, String> nullMapHandle(Map<String, String> map) {
		return map == null ? Collections.emptyMap() : map;
	}

	/**
	 * 上报属性地址
	 */
	private String reportPropertyPath;
	/**
	 * 固件更新地址
	 */
	private String firmwareProgressPath;
	/**
	 * 拉取固件更新地址
	 */
	private String firmwarePullPath;
}
