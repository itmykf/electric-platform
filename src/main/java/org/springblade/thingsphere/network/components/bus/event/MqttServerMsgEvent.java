package org.springblade.thingsphere.network.components.bus.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author lhb
 * @date 2024/9/29 下午2:59
 */
public class MqttServerMsgEvent<T> extends ApplicationEvent {

	private T msg;

	public MqttServerMsgEvent(Object source, T msg) {
		super(source);
		this.msg = msg;
	}

	public T getMsg() {
		return msg;
	}
}
