package org.springblade.thingsphere.network.components.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lhb
 * @date 2024/9/29 上午9:53
 */
@Data
public class NetworkProperties implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 配置ID
	 */
	private Long id;

	/**
	 * 配置名称
	 */
	private String name;

	/**
	 * 配置是否启用
	 */
	private boolean enabled;

	/**
	 * 配置内容，不同的网络组件，配置内容不同
	 */
	private String configuration;

}
