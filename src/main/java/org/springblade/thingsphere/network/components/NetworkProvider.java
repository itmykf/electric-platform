package org.springblade.thingsphere.network.components;

public interface NetworkProvider<P, T> {

	/**
	 * @return 类型
	 */
	NetworkType getType();

	/**
	 * 使用配置创建一个网络组件
	 *
	 * @return 网络组件
	 */
	T createNetwork(P config);

	/**
	 * 关闭网络组件
	 */
	void shutdown(Long id);
}
