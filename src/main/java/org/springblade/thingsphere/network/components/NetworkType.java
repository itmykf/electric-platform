package org.springblade.thingsphere.network.components;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lhb
 * @date 2024/9/26 上午9:55
 */
@AllArgsConstructor
@Getter
public enum NetworkType {

	TCP_CLIENT("TCP_CLIENT"),
	TCP_SERVER("TCP_SERVER"),

	MQTT_CLIENT("MQTT_CLIENT"),
	MQTT_SERVER("MQTT_SERVER"),

	HTTP_CLIENT("HTTP_CLIENT"),
	HTTP_SERVER("HTTP_SERVER"),

	WEB_SOCKET_CLIENT("WEB_SOCKET_CLIENT"),
	WEB_SOCKET_SERVER("WEB_SOCKET_SERVER"),

	UDP("UDP"),

	COAP_CLIENT("COAP_CLIENT"),
	COAP_SERVER("COAP_SERVER"),
	;

	private final String name;

}
