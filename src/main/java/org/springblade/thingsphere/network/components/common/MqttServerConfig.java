package org.springblade.thingsphere.network.components.common;

import lombok.*;

/**
 * @author lhb
 * @date 2024/9/29 上午10:01
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MqttServerConfig {

	/**
	 * 服务配置ID
	 */
	private Long id;

	/**
	 * 服务实例数量(线程数)
	 */
	private int threadNum = Math.max(4, Runtime.getRuntime().availableProcessors());

	/**
	 * 绑定端口
	 */
	private int port;

	/**
	 * 绑定网卡
	 */
	private String host = "0.0.0.0";

	/**
	 * 是否开启SSL
	 */
	private boolean ssl;

	/**
	 * 证书ID
	 *
	 * @see Certificate#getId()
	 * @see org.jetlinks.pro.network.security.CertificateManager
	 */
	private String certId;

	private String userName;
	private String password;

	/**
	 * 上报属性地址
	 */
	private String reportPropertyPath;
}
