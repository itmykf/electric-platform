package org.springblade.thingsphere.network.components;

import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttServer;
import org.springblade.thingsphere.network.components.http.server.VertxHttpServer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author lhb
 * @date 2024/9/29 上午11:19
 */
public class VertxInstance {
	// http服务 网络组件实例
	public static final Map<Long, VertxHttpServer> httpServerInstances = new ConcurrentHashMap<>();

	// mqtt客户端 网络组件实例
	public static final Map<Long, MqttClient> MqttClientInstances = new ConcurrentHashMap<>();

	// mqtt服务端 网络组件实例
	public static final Map<Long, MqttServer> MqttServerInstances = new ConcurrentHashMap<>();

}
