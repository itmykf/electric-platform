package org.springblade.thingsphere.network.components.common;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author lhb
 * @date 2024/9/29 上午9:55
 */
@Configuration
public class DeviceGatewayConfiguration {

	@ConfigurationProperties(prefix = "vertx")
	@Bean
	public VertxOptions vertxOptions() {
		int availableProcessors = Runtime.getRuntime().availableProcessors();
		return new VertxOptions()
			.setEventLoopPoolSize(availableProcessors * 2)
			.setWorkerPoolSize(availableProcessors * 2)
			.setInternalBlockingPoolSize(availableProcessors * 8)
			.setBlockedThreadCheckInterval(1000)
			.setWarningExceptionTime(2000)
			.setMaxEventLoopExecuteTime(30)
			.setMaxEventLoopExecuteTimeUnit(TimeUnit.SECONDS)
			.setMaxWorkerExecuteTime(30)
			.setMaxWorkerExecuteTimeUnit(TimeUnit.SECONDS)
			.setPreferNativeTransport(true);
	}

	@Bean
	public Vertx vertx(VertxOptions vertxOptions) {
		return Vertx.vertx(vertxOptions);
	}

}
