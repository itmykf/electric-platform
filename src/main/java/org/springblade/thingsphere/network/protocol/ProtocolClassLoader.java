package org.springblade.thingsphere.network.protocol;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author lhb
 * @date 2024/9/25 下午5:28
 */
public class ProtocolClassLoader extends URLClassLoader {
	private final URL[] urls;

	public ProtocolClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
		this.urls = urls;
	}

	public void close() throws IOException {
		super.close();
	}

	public URL[] getUrls() {
		return this.urls;
	}
}
