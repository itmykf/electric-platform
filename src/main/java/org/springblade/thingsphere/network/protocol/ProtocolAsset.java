package org.springblade.thingsphere.network.protocol;

import org.springblade.thingsphere.CompositeProtocolSupport;

public interface ProtocolAsset {

	/**
	 * 动态加载jar
	 *
	 * @param definition
	 * @return
	 */
	CompositeProtocolSupport load(ProtocolSupportDefinition definition);

	/**
	 * 卸载
	 */
	boolean unLoadJar(ProtocolSupportDefinition definition);
}
