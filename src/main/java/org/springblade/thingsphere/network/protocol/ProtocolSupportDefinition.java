package org.springblade.thingsphere.network.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lhb
 * @date 2024/9/25 下午5:29
 */
@Data
public class ProtocolSupportDefinition implements Serializable {
	private static final long serialVersionUID = -1L;
	private Long id;
	private String name;
	private String jarPath;
	private String clazzName;
	private String description;
}
