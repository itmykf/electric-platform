package org.springblade.thingsphere.network.initialize;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springblade.thingsphere.manage.pojo.entity.ComponentsEntity;
import org.springblade.thingsphere.manage.service.IComponentsService;
import org.springblade.thingsphere.network.components.NetworkType;
import org.springblade.thingsphere.network.components.bus.listener.HandlerListenerImpl;
import org.springblade.thingsphere.network.components.common.HttpServerConfig;
import org.springblade.thingsphere.network.components.common.MqttClientConfig;
import org.springblade.thingsphere.network.components.common.MqttServerConfig;
import org.springblade.thingsphere.network.components.http.server.VertxHttpServer;
import org.springblade.thingsphere.network.components.mqtt.client.VertxMqttClient;
import org.springblade.thingsphere.network.components.mqtt.server.VertxMqttServer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lhb
 * @date 2024/9/30 下午1:49
 */
@Order(2)
@Component
@AllArgsConstructor
public class LoadingNetwork implements ApplicationRunner {

	private static final Logger log = LoggerFactory.getLogger(LoadingNetwork.class);
	private final IComponentsService componentsService;
	private final VertxHttpServer vertxHttpServer;
	private final VertxMqttServer vertxMqttServer;
	private final VertxMqttClient vertxMqttClient;

	private final HandlerListenerImpl handlerListener;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// 查询网络组件
		List<ComponentsEntity> list = componentsService.list(Wrappers.<ComponentsEntity>lambdaQuery()
			.eq(ComponentsEntity::getStatus, 1)
		);
		if (CollectionUtils.isEmpty(list)) {
			return;
		}
		// 初始化
		for (ComponentsEntity components : list) {
			if (NetworkType.HTTP_SERVER.getName().equals(components.getType())) {
				HttpServerConfig httpServerConfig = JSONObject.parseObject(components.getConfiguration(), HttpServerConfig.class);
				httpServerConfig.setId(components.getId());
				vertxHttpServer.createNetwork(httpServerConfig);
			} else if (NetworkType.MQTT_SERVER.getName().equals(components.getType())) {
				MqttServerConfig httpServerConfig = JSONObject.parseObject(components.getConfiguration(), MqttServerConfig.class);
				httpServerConfig.setId(components.getId());
				vertxMqttServer.createNetwork(httpServerConfig);
			} else if (NetworkType.MQTT_CLIENT.getName().equals(components.getType())) {
				MqttClientConfig httpClientConfig = JSONObject.parseObject(components.getConfiguration(), MqttClientConfig.class);
				httpClientConfig.setId(components.getId());
				vertxMqttClient.createNetwork(httpClientConfig);
			}
		}
	}
}
