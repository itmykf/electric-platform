package org.springblade.ocpp.job;

import lombok.extern.slf4j.Slf4j;
import org.springblade.ocpp.WebSocketServer;

/**
 * @author lhb
 * @date 2024/9/20 下午3:52
 */
@Slf4j
public class OnLineClientPrintJob {

	public void run() {
		log.info("在线客户数:{}", WebSocketServer.onlineCount);
	}

}
