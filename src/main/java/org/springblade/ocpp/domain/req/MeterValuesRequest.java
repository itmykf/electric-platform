package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lhb
 * @date 2024/9/10 下午2:49
 */
@NoArgsConstructor
@Data
public class MeterValuesRequest extends BasePayload {

	@JsonProperty("connectorId")
	private Integer connectorId;
	@JsonProperty("transactionId")
	private Integer transactionId;
	@JsonProperty("meterValue")
	private List<MeterValueDTO> meterValue;

	@NoArgsConstructor
	@Data
	public static class MeterValueDTO {
		@JsonProperty("timestamp")
		private String timestamp;
		@JsonProperty("sampledValue")
		private List<SampledValueDTO> sampledValue;

		@NoArgsConstructor
		@Data
		public static class SampledValueDTO {
			@JsonProperty("value")
			private String value;
			@JsonProperty("context")
			private String context;
			@JsonProperty("format")
			private String format;
			@JsonProperty("measurand")
			private String measurand;
			@JsonProperty("phase")
			private String phase;
			@JsonProperty("location")
			private String location;
			@JsonProperty("unit")
			private String unit;
		}
	}
}
