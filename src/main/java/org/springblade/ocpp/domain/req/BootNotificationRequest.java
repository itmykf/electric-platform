package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author lhb
 * @date 2024/9/10 下午2:30
 */
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class BootNotificationRequest extends BasePayload {

	@JsonProperty("chargePointModel")
	private String chargePointModel;
	@JsonProperty("chargePointVendor")
	private String chargePointVendor;
	@JsonProperty("firmwareVersion")
	private String firmwareVersion;
	@JsonProperty("meterSerialNumber")
	private String meterSerialNumber;
	@JsonProperty("meterType")
	private String meterType;
}
