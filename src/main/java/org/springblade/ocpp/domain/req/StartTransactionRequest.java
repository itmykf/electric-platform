package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午2:48
 */
@NoArgsConstructor
@Data
public class StartTransactionRequest extends BasePayload {

	@JsonProperty("connectorId")
	private Integer connectorId;
	@JsonProperty("idTag")
	private String idTag;
	@JsonProperty("meterStart")
	private Integer meterStart;
	@JsonProperty("timestamp")
	private String timestamp;
}
