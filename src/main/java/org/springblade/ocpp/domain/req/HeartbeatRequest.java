package org.springblade.ocpp.domain.req;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午2:41
 */
@Data
public class HeartbeatRequest extends BasePayload {
}
