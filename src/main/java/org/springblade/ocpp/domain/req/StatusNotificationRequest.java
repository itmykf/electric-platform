package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午2:40
 */
@NoArgsConstructor
@Data
public class StatusNotificationRequest extends BasePayload {

	@JsonProperty("connectorId")
	private Integer connectorId;
	@JsonProperty("errorCode")
	private String errorCode;
	@JsonProperty("status")
	private String status;
	@JsonProperty("timestamp")
	private String timestamp;
}
