package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午2:44
 */
@NoArgsConstructor
@Data
public class AuthorizeRequest extends BasePayload {

	@JsonProperty("idTag")
	private String idTag;
}
