package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author lhb
 * @date 2024/9/10 下午2:48
 */
@Data
public class RemoteStopTransactionRequest extends BasePayload implements Delayed {

	@JsonProperty("transactionId")
	private Long transactionId;

	private long expirationTime;

	public RemoteStopTransactionRequest() {
	}

	public RemoteStopTransactionRequest(int delaySeconds) {
		this.expirationTime = System.currentTimeMillis() + (delaySeconds * 1000);
	}

	@Override
	public long getDelay(@NotNull TimeUnit unit) {
		long diff = this.expirationTime - System.currentTimeMillis();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

	@Override
	public int compareTo(@NotNull Delayed o) {
		if (this.getDelay(TimeUnit.MILLISECONDS) < o.getDelay(TimeUnit.MILLISECONDS)) {
			return -1;
		}
		if (this.getDelay(TimeUnit.MILLISECONDS) > o.getDelay(TimeUnit.MILLISECONDS)) {
			return 1;
		}
		return 0;
	}
}
