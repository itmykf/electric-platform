package org.springblade.ocpp.domain.req;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class BasePayload {
	// 消息唯一ID
	public String uniqueId;
	// 消息类型
	public String action;

	// 充电桩ID
	private String sid;

}
