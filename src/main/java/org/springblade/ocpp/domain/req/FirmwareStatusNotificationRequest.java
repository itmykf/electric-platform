package org.springblade.ocpp.domain.req;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/7 下午2:58
 */
@Data
public class FirmwareStatusNotificationRequest extends BasePayload {

	private String status;

}
