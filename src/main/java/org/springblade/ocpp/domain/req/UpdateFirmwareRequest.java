package org.springblade.ocpp.domain.req;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/10/7 下午2:56
 */
@Data
public class UpdateFirmwareRequest extends BasePayload {

	// {"type":"","url":"https://114.115.210.66:9000/ocppManager/static/OTA-0508.zip"}
	private String location;
	private String retrieveDate;
}
