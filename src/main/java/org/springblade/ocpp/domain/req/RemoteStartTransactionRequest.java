package org.springblade.ocpp.domain.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author lhb
 * @date 2024/9/10 下午2:43
 */
@Data
public class RemoteStartTransactionRequest extends BasePayload implements Delayed {

	@JsonProperty("idTag")
	private String idTag;
	@JsonProperty("connectorId")
	private Integer connectorId;
	@JsonProperty("chargingProfile")
	private ChargingProfileDTO chargingProfile;

	private long expirationTime;

	public RemoteStartTransactionRequest() {
	}

	public RemoteStartTransactionRequest(int delaySeconds) {
		this.expirationTime = System.currentTimeMillis() + (delaySeconds * 1000);
	}

	@Override
	public long getDelay(@NotNull TimeUnit unit) {
		long diff = this.expirationTime - System.currentTimeMillis();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

	@Override
	public int compareTo(@NotNull Delayed o) {
		if (this.getDelay(TimeUnit.MILLISECONDS) < o.getDelay(TimeUnit.MILLISECONDS)) {
			return -1;
		}
		if (this.getDelay(TimeUnit.MILLISECONDS) > o.getDelay(TimeUnit.MILLISECONDS)) {
			return 1;
		}
		return 0;
	}

	@NoArgsConstructor
	@Data
	public static class ChargingProfileDTO {
		@JsonProperty("chargeProfileId")
		private Integer chargeProfileId;
		@JsonProperty("chargingProfileKind")
		private String chargingProfileKind;
		@JsonProperty("chargingProfilePurpose")
		private String chargingProfilePurpose;
		@JsonProperty("chargingSchedule")
		private ChargingScheduleDTO chargingSchedule;
		@JsonProperty("stackLevel")
		private Integer stackLevel;

		@NoArgsConstructor
		@Data
		public static class ChargingScheduleDTO {
			@JsonProperty("chargingRateUnit")
			private String chargingRateUnit;
			@JsonProperty("chargingSchedulePeriod")
			private List<ChargingSchedulePeriodDTO> chargingSchedulePeriod;

			@NoArgsConstructor
			@Data
			public static class ChargingSchedulePeriodDTO {
				@JsonProperty("limit")
				private Integer limit;
				@JsonProperty("startPeriod")
				private Integer startPeriod;
			}
		}
	}
}
