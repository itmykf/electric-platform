package org.springblade.ocpp.domain.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午4:09
 */
@NoArgsConstructor
@Data
public class AuthorizeConfirmation extends BaseRes {


	@JsonProperty("idTagInfo")
	private IdTagInfoDTO idTagInfo;

	@NoArgsConstructor
	@Data
	public static class IdTagInfoDTO {
		@JsonProperty("status")
		private String status;
		private String expiryDate;
		private String parentIdTag;
	}
}
