package org.springblade.ocpp.domain.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午4:11
 */
@Data
public class StopTransactionConfirmation extends BaseRes {

	@JsonProperty("idTagInfo")
	private StartTransactionConfirmation.IdTagInfoDTO idTagInfo;
	@JsonProperty("transactionId")
	private Long transactionId;

	@NoArgsConstructor
	@Data
	public static class IdTagInfoDTO {
		@JsonProperty("status")
		private String status;
		private String expiryDate;
		private String parentIdTag;
	}

}
