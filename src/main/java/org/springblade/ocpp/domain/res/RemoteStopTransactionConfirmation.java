package org.springblade.ocpp.domain.res;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午4:20
 */
@Data
public class RemoteStopTransactionConfirmation extends BaseRes {
	private String status;
}
