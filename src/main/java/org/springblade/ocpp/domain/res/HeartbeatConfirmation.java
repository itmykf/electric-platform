package org.springblade.ocpp.domain.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午4:10
 */
@Data
public class HeartbeatConfirmation extends BaseRes {
	@JsonProperty("currentTime")
	private String currentTime;
}
