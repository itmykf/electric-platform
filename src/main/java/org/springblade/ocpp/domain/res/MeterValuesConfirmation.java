package org.springblade.ocpp.domain.res;

import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午4:09
 */
@Data
public class MeterValuesConfirmation extends BaseRes {
}
