package org.springblade.ocpp.domain.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lhb
 * @date 2024/9/10 下午4:09
 */
@NoArgsConstructor
@Data
public class BootNotificationConfirmation extends BaseRes {

	@JsonProperty("currentTime")
	private String currentTime;
	@JsonProperty("interval")
	private Integer interval;
	@JsonProperty("status")
	private String status;
}
