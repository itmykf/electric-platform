package org.springblade.ocpp.domain.res;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午4:10
 */
@Data
@AllArgsConstructor
public class DataTransferConfirmation extends BaseRes {

	private String status;
}
