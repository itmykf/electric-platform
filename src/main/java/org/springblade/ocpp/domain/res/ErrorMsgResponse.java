package org.springblade.ocpp.domain.res;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author lhb
 * @date 2024/9/10 下午3:01
 */
@Data
@AllArgsConstructor
public class ErrorMsgResponse extends BaseRes {

	// 消息类型
	protected String message;


}
