package org.springblade.ocpp.handle;


import org.springblade.ocpp.domain.req.*;
import org.springblade.ocpp.domain.res.*;

public interface ServerCoreEventHandler {

	/**
	 * 处理授权请求
	 *
	 * @param request
	 * @return
	 */
	AuthorizeConfirmation handleAuthorizeRequest(AuthorizeRequest request);

	/**
	 * 处理引导通知请求
	 *
	 * @param request
	 * @return
	 */

	BootNotificationConfirmation handleBootNotificationRequest(BootNotificationRequest request);

	/**
	 * 处理数据传输请求
	 *
	 * @param request
	 * @return
	 */
	DataTransferConfirmation handleDataTransferRequest(DataTransferRequest request);

	/**
	 * 处理心跳请求
	 *
	 * @param request
	 * @return
	 */
	HeartbeatConfirmation handleHeartbeatRequest(HeartbeatRequest request);

	/**
	 * 处理仪表值请求
	 *
	 * @param request
	 * @return
	 */
	MeterValuesConfirmation handleMeterValuesRequest(MeterValuesRequest request);

	/**
	 * 处理启动事务请求 (启动充电)
	 *
	 * @param request
	 * @return
	 */
	StartTransactionConfirmation handleStartTransactionRequest(StartTransactionRequest request);

	/**
	 * 处理停止事务请求 (停止充电)
	 *
	 * @param request
	 * @return
	 */
	StopTransactionConfirmation handleStopTransactionRequest(StopTransactionRequest request);

	/**
	 * 处理状态通知请求
	 *
	 * @param request
	 * @return
	 */
	StatusNotificationConfirmation handleStatusNotificationRequest(StatusNotificationRequest request);

	/**
	 * OTA升级, 客户端上报升级状态
	 *
	 * @param request
	 * @return
	 */
	FirmwareStatusNotificationConfirmation handleFirmwareStatusNotification(FirmwareStatusNotificationRequest request);
}
