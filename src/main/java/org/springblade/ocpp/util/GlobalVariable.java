package org.springblade.ocpp.util;

import cn.hutool.core.util.StrUtil;

/**
 * @author lhb
 * @date 2024/9/10 下午2:23
 */
public class GlobalVariable {

	public enum ActionType {
		BOOT_NOTIFICATION("BootNotification", "通知"),

		STATUS_NOTIFICATION("StatusNotification", "状态上报"),

		HEART_BEAT("Heartbeat", "心跳"),

		REMOTE_START_TRANSACTION("RemoteStartTransaction", "远程开始充电"),
		START_TRANSACTION("StartTransaction", "开始充电"),

		REMOTE_STOP_TRANSACTION("RemoteStopTransaction", "远程结束充电"),
		STOP_TRANSACTION("StopTransaction", "结束充电"),

		AUTHORIZE("Authorize", "鉴权"),

		METER_VALUES("MeterValues", "记值"),

		UPDATE_FIRMWARE("UpdateFirmware", "固件升级"),

		FIRMWARE_STATUS_NOTIFICATION("FirmwareStatusNotification", "固件升级状态"),

		DATA_TRANSFER("DataTransfer", "定时充电"),
		;

		ActionType(String value, String msg) {
			this.value = value;
			this.msg = msg;
		}

		final String value;
		final String msg;

		public String value() {
			return value;
		}

		public String msg() {
			return msg;
		}

		public static ActionType toEnum(String value) {
			if (StrUtil.isBlank(value)) {
				return null;
			}
			ActionType result = null;
			for (ActionType val : values()) {
				if (value.equals(val.value())) {
					result = val;
					break;
				}
			}
			return result;
		}
	}

	public enum Status {

		Accepted("Accepted", "同意"),
		Finishing("Finishing", "完成"),
		Preparing("Preparing", "准备"),
		Available("Available", "可用"),
		Charging("Charging", "充电"),
		Downloading("Downloading", "下载中"),
		Installing("Installing", "安装中"),
		Installed("Installed", "安装完成"),
		;

		Status(String value, String msg) {
			this.value = value;
			this.msg = msg;
		}

		final String value;
		final String msg;

		public String value() {
			return value;
		}

		public String msg() {
			return msg;
		}

		public static Status toEnum(String value) {
			if (StrUtil.isBlank(value)) {
				return null;
			}
			Status result = null;
			for (Status val : values()) {
				if (value.equals(val.value())) {
					result = val;
					break;
				}
			}
			return result;
		}
	}


}
