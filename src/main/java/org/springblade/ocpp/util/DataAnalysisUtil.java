package org.springblade.ocpp.util;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springblade.ocpp.domain.req.*;
import org.springblade.ocpp.domain.res.BaseRes;
import org.springblade.ocpp.domain.res.ErrorMsgResponse;
import org.springblade.ocpp.handle.ServerCoreEventHandler;

/**
 * @author lhb
 * @date 2024/9/10 下午2:25
 */
@Slf4j
public class DataAnalysisUtil {

	/**
	 * 请求参数数据入参
	 *
	 * @param msg 原始数据
	 * @return 解析后并封装的数据
	 */
	public static BasePayload reqDataAnalysis(String msg, String sid) {
		JSONArray objects = JSONArray.parse(msg);
		Integer messageType = (Integer) objects.get(0);
		// 如果是客户端回复的消息, 另外处理(这里匹配服务端上次发送的数据是否存在uniqueId一样的数据, 如果在则处理此项, 没业务也可以不处理)
		if (3 == messageType) {
			return null;
		}
		String uniqueId = (String) objects.get(1);
		String action = (String) objects.get(2);
		JSONObject payload = objects.getJSONObject(3);

		GlobalVariable.ActionType anEnum = GlobalVariable.ActionType.toEnum(action);
		if (GlobalVariable.ActionType.BOOT_NOTIFICATION == anEnum) {
			return payload.toJavaObject(BootNotificationRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.STATUS_NOTIFICATION == anEnum) {
			return payload.toJavaObject(StatusNotificationRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.HEART_BEAT == anEnum) {
			return payload.toJavaObject(HeartbeatRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.START_TRANSACTION == anEnum) {
			return payload.toJavaObject(StartTransactionRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.STOP_TRANSACTION == anEnum) {
			return payload.toJavaObject(StopTransactionRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.AUTHORIZE == anEnum) {
			return payload.toJavaObject(AuthorizeRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.METER_VALUES == anEnum) {
			return payload.toJavaObject(MeterValuesRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.DATA_TRANSFER == anEnum) {
			return payload.toJavaObject(DataTransferRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		} else if (GlobalVariable.ActionType.FIRMWARE_STATUS_NOTIFICATION == anEnum) {
			return payload.toJavaObject(FirmwareStatusNotificationRequest.class).setUniqueId(uniqueId).setAction(action).setSid(sid);
		}
		return null;
	}

	public static String resDataAnalysis(ServerCoreEventHandler serverCoreEventHandler, BasePayload payload) {
		if (null == payload) {
			return null;
		}
		BaseRes response = null;
		JSONArray res = new JSONArray();
		try {
			if (payload instanceof BootNotificationRequest) {
				response = serverCoreEventHandler.handleBootNotificationRequest((BootNotificationRequest) payload);
			} else if (payload instanceof StatusNotificationRequest) {
				response = serverCoreEventHandler.handleStatusNotificationRequest((StatusNotificationRequest) payload);
			} else if (payload instanceof HeartbeatRequest) {
				response = serverCoreEventHandler.handleHeartbeatRequest((HeartbeatRequest) payload);
			} else if (payload instanceof StartTransactionRequest) {
				response = serverCoreEventHandler.handleStartTransactionRequest((StartTransactionRequest) payload);
			} else if (payload instanceof StopTransactionRequest) {
				response = serverCoreEventHandler.handleStopTransactionRequest((StopTransactionRequest) payload);
			} else if (payload instanceof AuthorizeRequest) {
				response = serverCoreEventHandler.handleAuthorizeRequest((AuthorizeRequest) payload);
			} else if (payload instanceof MeterValuesRequest) {
				response = serverCoreEventHandler.handleMeterValuesRequest((MeterValuesRequest) payload);
			} else if (payload instanceof DataTransferRequest) {
				response = serverCoreEventHandler.handleDataTransferRequest((DataTransferRequest) payload);
			} else if (payload instanceof FirmwareStatusNotificationRequest) {
				response = serverCoreEventHandler.handleFirmwareStatusNotification((FirmwareStatusNotificationRequest) payload);
			}
			res.add(0, 3);
			res.add(1, payload.getUniqueId());
			res.add(2, response);
		} catch (Exception e) {
			res.add(0, 4);
			res.add(1, payload.getUniqueId());
			res.add(2, new ErrorMsgResponse(e.getMessage()));
			log.error("resDataAnalysis err", e);
		}
		return res.toString();
	}

	public static String serverToClientRes(BasePayload payload, String key) {
		JSONArray res = new JSONArray();
		res.add(0, 2);
		res.add(1, IdUtil.fastSimpleUUID());
		res.add(2, key);
		res.add(3, payload);
		return res.toString();
	}

}
