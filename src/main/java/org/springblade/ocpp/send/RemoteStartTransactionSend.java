package org.springblade.ocpp.send;

import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springblade.ocpp.WebSocketServer;
import org.springblade.ocpp.domain.req.RemoteStartTransactionRequest;
import org.springblade.ocpp.util.DataAnalysisUtil;
import org.springblade.ocpp.util.GlobalVariable;

/**
 * @author lhb
 * @date 2024/10/5 下午2:47
 */
@Slf4j
public class RemoteStartTransactionSend {

	/**
	 * 发送远程开始充电请求
	 *
	 * @param request
	 */
	public static void send(RemoteStartTransactionRequest request) {
		log.info("[RemoteStartTransaction]发送远程开始充电:{} sid:{}", JSONObject.toJSONString(request), request.getSid());
		WebSocketServer.sendInfo(DataAnalysisUtil.serverToClientRes(request, GlobalVariable.ActionType.REMOTE_START_TRANSACTION.value()), request.getSid());
	}
}
