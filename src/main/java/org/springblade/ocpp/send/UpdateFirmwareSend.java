package org.springblade.ocpp.send;

import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springblade.ocpp.WebSocketServer;
import org.springblade.ocpp.domain.req.UpdateFirmwareRequest;
import org.springblade.ocpp.util.DataAnalysisUtil;
import org.springblade.ocpp.util.GlobalVariable;

/**
 * @author lhb
 * @date 2024/10/7 下午3:03
 */
@Slf4j
public class UpdateFirmwareSend {

	/**
	 * 发送远程开始充电请求
	 *
	 * @param request
	 */
	public static void send(UpdateFirmwareRequest request) {
		log.info("[RemoteStopTransaction]发送远程开始充电:{} sid:{}", JSONObject.toJSONString(request), request.getSid());
		WebSocketServer.sendInfo(DataAnalysisUtil.serverToClientRes(request, GlobalVariable.ActionType.UPDATE_FIRMWARE.value()), request.getSid());
	}
}
