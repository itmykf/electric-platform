package org.springblade.ocpp.send;

import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springblade.ocpp.WebSocketServer;
import org.springblade.ocpp.domain.req.DataTransferRequest;
import org.springblade.ocpp.util.DataAnalysisUtil;
import org.springblade.ocpp.util.GlobalVariable;

/**
 * @author lhb
 * @date 2024/10/5 下午3:56
 */
@Slf4j
public class DataTransferSend {

	/**
	 * 发送远程开始充电请求
	 *
	 * @param request
	 */
	public static void send(DataTransferRequest request) {
		log.info("[DataTransfer]发送数据给充电桩:{} sid:{}", JSONObject.toJSONString(request), request.getSid());
		WebSocketServer.sendInfo(DataAnalysisUtil.serverToClientRes(request, GlobalVariable.ActionType.DATA_TRANSFER.value()), request.getSid());
	}

}
